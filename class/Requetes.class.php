<?php
class Requetes {

    private $Tables; //Array

    public function __construct () 
    {
		$this->Tables = [
            1 => "Sejour_Hospitalisation",
            2 => "Maladie",
            3 => "Remede",
            4 => "guerir",
            5 => "rembourser",
            6 => "Mutuelle",
            7 => "Patient",
            8 => "Consultation",
            9 => "Laboratoire",
            10 => "Hopital",
            11 => "TypeExamenMedical",
            12 => "Service"
          ];
    }
    
    public function connect()  // initialisation de la connection à la base de données 
    {
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dBName = "sante";
        try {
            $options = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'  // pour les caractères spéciaux (accents, ...)
              );
            $conn = new PDO("mysql:host=$servername;dbname=$dBName", $username, $password, $options);
            
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        catch(PDOException $e)
            {
            echo "Connection failed: " . $e->getMessage();
            die();
            }

        return $conn;
    }
    
    public function getTable() // effectue les requetes pour récupérer les données permettant l'insertion
    {
        return $this->Tables;
    }

    public function getKeys(string $key_table)
    {
        $conn = $this->connect();
        $table_name = $this->Tables[$key_table];
        // on récupère le nom des champs et le type de données sauf pour les clés étrangères
        $sql = <<<SQL
            SELECT COLUMN_NAME as COL,DATA_TYPE as DT
            FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = "$table_name" AND COLUMN_NAME NOT IN (
                        SELECT COLUMN_NAME 
                        FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE 
                        WHERE TABLE_NAME= "$table_name" AND REFERENCED_TABLE_NAME IS NOT NULL
                        );
SQL;

        $query = $conn->query($sql);
        $resp1 = $query->fetchAll();
        // on récupère les infos sur les clés étrangères
        $sql2 = <<<SQL
            SELECT REFERENCED_TABLE_NAME,COLUMN_NAME 
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE 
            WHERE TABLE_NAME= "$table_name" AND REFERENCED_TABLE_NAME IS NOT NULL;
SQL;

        $query = $conn->query($sql2);
        $resp2 = $query->fetchAll();
        
        $res = [];
        $res['nofkeys'] = [];
        $res['fkeys'] = [];
        $res['table_name'] = $table_name;

        $tableFkRef = [];

        foreach($resp2 as $elem)
        {
        
            $tableFkRef[$elem['COLUMN_NAME']] = $elem['REFERENCED_TABLE_NAME'];
        
        }

        foreach ($resp1 as $elem)
        {
            if ($elem['DT'] === 'date')
            {
                //$elem['DT'] = "date". ": aaaa-mm-dd" ;
                $res['nofkeys'][]=[
                    'name'=>$elem['COL'],
                    'dataType'=>$elem['DT'],
                    'detail'=> "aaaa-mm-jj"
                ];
            }
            elseif ($elem['DT'] === 'datetime')
            {
                //$elem['DT'] = "datetime". ": aaaa-mm-dd  hh:mm:ss" ;
                $res['nofkeys'][]=[
                    'name'=>$elem['COL'],
                    'dataType'=>$elem['DT'],
                    'detail'=> "aaaa-mm-jj hh:mm:ss"
                ];
            }
            else
            {
                $res['nofkeys'][]=[
                    'name'=>$elem['COL'],
                    'dataType'=>$elem['DT'],
                    'detail'=>'  '
                ];
            }
            
        };

        $res['fkeys']=$tableFkRef;
       
        $conn = null;
        
        return $res;
        
    }

    public function insert($post) // on poste les informations envoyées dans le formulaire
    {
        try
        {
        $keysStr = '';
        $valuesStr = '';
        
        if (in_array("graviteMal",array_keys($post))){
            
            if (!in_array($post["graviteMal"], ["BEN","SER","CRI"])){
                throw new Exception("mauvaise valeur de graviteMal");
            }
            ;
        }
        $table_name = htmlentities($post['table_name']);

        $columns = array_slice($post,1);
        $columnString = implode(',', array_keys($columns));
        $valueString = implode(',', array_fill(0, count($columns), '?'));

        $conn = $this->connect();
        $query = $conn->prepare("INSERT INTO $table_name ({$columnString}) VALUES ({$valueString})");
        $query->execute(array_values($columns));
        $conn = null;
        header("Location: inserer.php?success=success");
        
        } 
        catch(PDOException | Exception $e)
            {
                $erreurMessage = $e->getMessage();
                $alert = <<<HTML
                <div class="alert alert-warning">
                    <strong>Warning!</strong> Une erreur est survenue lors de l'insertion. Veuillez reessayer.
                    <p>$erreurMessage</p>
                </div>
HTML;
                echo $alert;
                die();
            }

    }

    public function resValues(string $id, string $table)// récupère les id d'une certaine table pour les clés étrangères
    {
        $conn = $this->connect();

        $sql = <<<SQL
        select COLUMN_NAME from information_schema.columns  where TABLE_NAME="$table" and (column_name like 'nom%');
SQL;
        $query = $conn->query($sql);
        $req = $query->fetch();
        $nom = $req["COLUMN_NAME"];

        $sql = <<<SQL
        SELECT $id,$nom FROM $table
SQL;
        $query = $conn->query($sql);
        $req = $query->fetchAll();
        
        $res_values = [];
        foreach ($req as $ide => $elem)
        {
            $res_values[]=[
                'id'=>$elem[0],
                'nom'=>$elem[1]
            ];
        };
        $conn = null;
        return $res_values;
    }
};

