
insert into Consultation (idConsult,idMed,idPat,dateHConsult)
values
(1,3310,25,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(2,3311,26,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(3,3312,27,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(4,3313,28,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(5,3314,29,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(6,3315,51,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(7,3316,52,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(8,3317,53,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(9,3318,54,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i')),
(10,3319,55,STR_TO_DATE('17/02/2012 11:30','%d/%m/%Y %H:%i'));


INSERT INTO `ExamenMedical` (`idExam`, `idTpExam`, `idPat`, `idLab`, `dateHExam`) VALUES ('20', '1140', '52', '1626', '2019-12-26 04:10:00'), ('21', '1141', '2744', '1626', '2019-12-27 08:00:00'), ('22', '1143', '2742', '1624', '2019-12-28 15:00:00'), ('23', '1141', '54', '1625', '2019-12-30 14:00:00'), ('24', '1140', '51', '1623', '2019-12-31 18:00:00') ;


INSERT INTO Service (idSer, idHopital, idMed, nomSer) VALUES       
(1630,1,3310, 'Pédiatrie'),
(1631,1,3313, 'Ophtalmologie'),
(1632,2,3313, 'Gynécologie'),
(1633,1,3310, 'Oncologie gynécologique'),
(1634,3,3313, 'Pédiatrie'),
(1635,4,3310, 'Ophtalmologie'),
(1636,4,3310, 'Pédiatrie'),
(1637,5,3311, 'Urologie'),
(1638,1,3311, 'Neurochirurgie');

INSERT INTO Laboratoire VALUES
(1628,1,'Labo alpha',NULL,NULL,'Reims'),
(1629,2,'Labo alpha',NULL,NULL,'Reims'),
(1630,3,'Labo alpha',NULL,NULL,'Toul'),
(1631,1,'Labo alpha',NULL,NULL,'Reims'),
(1632,1,'Labo alpha',NULL,NULL,'Reims'),
(1633,1,'Labo alpha',NULL,NULL,'Reims');
