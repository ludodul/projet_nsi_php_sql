USE projet4;

DROP Table IF EXISTS Prescrire,Rembourser,Remede,Maladie,Consultation,Patient,Mutuelle;



CREATE TABLE Mutuelle
(
    IdMut INT NOT NULL PRIMARY KEY,
    NomMut NVARCHAR(50) NOT NULL,
    AdresseMut NVARCHAR(200) NOT NULL,
    CPMut INT NOT NULL,
    VilleMut NVARCHAR(50) NOT NULL
);


CREATE TABLE Maladie
(
    IdMal INT NOT NULL PRIMARY KEY,
    NomMal NVARCHAR(50) NOT NULL,
    GraviteMal ENUM("Bénigne","Sérieuse","Critique")   
);

CREATE TABLE Remede
(
    IdRem INT NOT NULL PRIMARY KEY,
    NomRem NVARCHAR(50) NOT NULL,
    IdMal INT NOT NULL,
    FOREIGN KEY (IdMal) REFERENCES Maladie(IdMal)
       
);

CREATE TABLE Patient
(
    IdPatient INT NOT NULL PRIMARY KEY,
    NomPatient NVARCHAR(50) NOT NULL,
    PrenomPatient NVARCHAR(50) NOT NULL,
    AdressePatient NVARCHAR(200) NOT NULL,
    CPPatient INT NOT NULL,
    VillePatient NVARCHAR(50) NOT NULL,
    IdMut INT,
    FOREIGN KEY (IdMut) REFERENCES Mutuelle(IdMut)  
);

CREATE TABLE Consultation
(
    IdConsult INT NOT NULL PRIMARY KEY,
    IdPatient INT NOT NULL,
    FOREIGN KEY (IdPatient) REFERENCES Patient(IdPatient),
    DateConsult DATETIME NOT NULL
     
);

CREATE TABLE Rembourser
(
    IdRem INT NOT NULL,    
    IdMut INT NOT NULL,
    CONSTRAINT IdRemb PRIMARY KEY (IdRem, IdMut),
    FOREIGN KEY (IdRem) REFERENCES Remede(IdRem),
    FOREIGN KEY (IdMut) REFERENCES Mutuelle(IdMut),    
    Taux INT NOT NULL,
    CHECK (Taux>=0 AND Taux<=100)   
        
);


CREATE TABLE Prescrire
(
    IdRem INT NOT NULL,    
    IdConsult INT NOT NULL,
    PRIMARY KEY (IdRem, IdConsult),
    FOREIGN KEY (IdConsult) REFERENCES Consultation(IdConsult),
    FOREIGN KEY (IdRem) REFERENCES Remede(IdRem),
    Quantite INT NOT NULL,
    CHECK (Quantite>0)           
);

;



