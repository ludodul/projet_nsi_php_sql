/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  20/12/2019 07:58:14                      */
/*==============================================================*/

/*
alter table Consultation
   drop FOREIGN KEY  FK_realiser;

alter table Consultation
   drop FOREIGN KEY  FK_suivre;

alter table ExamenMedical
   drop FOREIGN KEY  FK_effectuer;

alter table ExamenMedical
   drop FOREIGN KEY  FK_proceder;

alter table ExamenMedical
   drop FOREIGN KEY  FK_subir;

alter table Laboratoire
   drop FOREIGN KEY  FK_dependre;

alter table Patient
   drop FOREIGN KEY  FK_cotiser;

alter table Patient
   drop FOREIGN KEY  FK_declarer_traitant;

alter table Sejour_Hospitalisation
   drop FOREIGN KEY  FK_aller;

alter table Sejour_Hospitalisation
   drop FOREIGN KEY  FK_demander;

alter table Sejour_Hospitalisation
   drop FOREIGN KEY  FK_localiser;

alter table Service
   drop FOREIGN KEY  FK_diriger;

alter table Service
   drop FOREIGN KEY  FK_situer;

alter table exercer
   drop FOREIGN KEY  FK_exercer;

alter table exercer
   drop FOREIGN KEY  FK_exercer2;

alter table guerir
   drop FOREIGN KEY  FK_guerir;

alter table guerir
   drop FOREIGN KEY  FK_guerir2;

alter table prescrireExam
   drop FOREIGN KEY  FK_prescrireExam;

alter table prescrireExam
   drop FOREIGN KEY  FK_prescrireExam2;

alter table prescrireMedoc
   drop FOREIGN KEY  FK_prescrireMedoc;

alter table prescrireMedoc
   drop FOREIGN KEY  FK_prescrireMedoc2;

alter table rembourser
   drop FOREIGN KEY  FK_rembourser;

alter table rembourser
   drop FOREIGN KEY  FK_rembourser2;

alter table reverser
   drop FOREIGN KEY  FK_reverser;

alter table reverser
   drop FOREIGN KEY  FK_reverser2;

drop table if exists Consultation;

drop table if exists ExamenMedical;

drop table if exists Hopital;

drop table if exists Laboratoire;

drop table if exists Maladie;

drop table if exists Medecin;

drop table if exists Mutuelle;

drop table if exists Patient;

drop table if exists Remede;

drop table if exists Sejour_Hospitalisation;

drop table if exists Service;

drop table if exists TypeExamenMedical;

drop table if exists exercer;

drop table if exists guerir;

drop table if exists prescrireExam;

drop table if exists prescrireMedoc;

drop table if exists rembourser;

drop table if exists reverser;
*/

/*==============================================================*/
/* Table : Consultation                                         */
/*==============================================================*/
create table Consultation
(
   idConsult            int not null,
   idMed                int not null,
   idPat                int not null,
   dateHConsult         datetime not null,
   primary key (idConsult)
);

/*==============================================================*/
/* Table : ExamenMedical                                        */
/*==============================================================*/
create table ExamenMedical
(
   idExam               int not null,
   idTpExam             int not null,
   idPat                int not null,
   idLab                int not null,
   dateHExam            datetime not null,
   primary key (idExam)
);

/*==============================================================*/
/* Table : Hopital                                              */
/*==============================================================*/
create table Hopital
(
   idHopital            int not null,
   nomHop               varchar(100) not null,
   adHop                varchar(200) not null,
   CPHop                varchar(6) not null,
   villeHop             varchar(100) not null,
   primary key (idHopital)
);

/*==============================================================*/
/* Table : Laboratoire                                          */
/*==============================================================*/
create table Laboratoire
(
   idLab                int not null,
   idHopital            int,
   nomLab               varchar(100) not null,
   adLab                varchar(200),
   CPLab                varchar(6),
   villeLab             varchar(100),
   primary key (idLab)
);

/*==============================================================*/
/* Table : Maladie                                              */
/*==============================================================*/
create table Maladie
(
   idMaladie            int not null,
   nomMal               varchar(100) not null,
   graviteMal           char(3) not null,
   primary key (idMaladie)
);

/*==============================================================*/
/* Table : Medecin                                              */
/*==============================================================*/
create table Medecin
(
   idMed                int not null,
   nomMed               varchar(100) not null,
   pnomMed              varchar(100) not null,
   adMed                varchar(200),
   CPMed                varchar(6),
   villeMed             varchar(100),
   primary key (idMed)
);

/*==============================================================*/
/* Table : Mutuelle                                             */
/*==============================================================*/
create table Mutuelle
(
   idMut                int not null,
   nomMut               varchar(100) not null,
   adMut                varchar(200) not null,
   CPMut                varchar(6) not null,
   villeMut             varchar(100) not null,
   primary key (idMut)
);

/*==============================================================*/
/* Table : Patient                                              */
/*==============================================================*/
create table Patient
(
   idPat                int not null,
   idMut                int not null,
   idMed                int,
   nomPat               varchar(100) not null,
   pnomPat              varchar(100) not null,
   dateNais             date not null,
   adPat                varchar(200) not null,
   CPPat                varchar(6) not null,
   villePat             varchar(100) not null,
   primary key (idPat)
);

/*==============================================================*/
/* Table : Remede                                               */
/*==============================================================*/
create table Remede
(
   idRem                int not null,
   nomRem               varchar(100) not null,
   primary key (idRem)
);

/*==============================================================*/
/* Table : Sejour_Hospitalisation                               */
/*==============================================================*/
create table Sejour_Hospitalisation
(
   idSejour             int not null,
   idPat                int not null,
   idMed                int not null,
   idSer                int not null,
   dateHAdmissiion      datetime not null,
   dateSortie           datetime not null,
   primary key (idSejour)
);

/*==============================================================*/
/* Table : Service                                              */
/*==============================================================*/
create table Service
(
   idSer                int not null,
   idHopital            int not null,
   idMed                int not null,
   nomSer               varchar(100) not null,
   primary key (idSer)
);

/*==============================================================*/
/* Table : TypeExamenMedical                                    */
/*==============================================================*/
create table TypeExamenMedical
(
   idTpExam             int not null,
   nomTpExam            varchar(100) not null,
   primary key (idTpExam)
);

/*==============================================================*/
/* Table : exercer                                              */
/*==============================================================*/
create table exercer
(
   idSer                int not null,
   idMed                int not null,
   primary key (idSer, idMed)
);

/*==============================================================*/
/* Table : guerir                                               */
/*==============================================================*/
create table guerir
(
   idMaladie            int not null,
   idRem                int not null,
   primary key (idMaladie, idRem)
);

/*==============================================================*/
/* Table : prescrireExam                                        */
/*==============================================================*/
create table prescrireExam
(
   idConsult            int not null,
   idTpExam             int not null,
   primary key (idConsult, idTpExam)
);

/*==============================================================*/
/* Table : prescrireMedoc                                       */
/*==============================================================*/
create table prescrireMedoc
(
   idConsult            int not null,
   idRem                int not null,
   primary key (idConsult, idRem)
);

/*==============================================================*/
/* Table : rembourser                                           */
/*==============================================================*/
create table rembourser
(
   idRem                int not null,
   idMut                int not null,
   quotiteRem           float,
   primary key (idRem, idMut)
);

/*==============================================================*/
/* Table : reverser                                             */
/*==============================================================*/
create table reverser
(
   idTpExam             int not null,
   idMut                int not null,
   quotieTpExam         float,
   primary key (idTpExam, idMut)
);

alter table Consultation add constraint FK_realiser foreign key (idMed)
      references Medecin (idMed) on delete restrict on update restrict;

alter table Consultation add constraint FK_suivre foreign key (idPat)
      references Patient (idPat) on delete restrict on update restrict;

alter table ExamenMedical add constraint FK_effectuer foreign key (idLab)
      references Laboratoire (idLab) on delete restrict on update restrict;

alter table ExamenMedical add constraint FK_proceder foreign key (idTpExam)
      references TypeExamenMedical (idTpExam) on delete restrict on update restrict;

alter table ExamenMedical add constraint FK_subir foreign key (idPat)
      references Patient (idPat) on delete restrict on update restrict;

alter table Laboratoire add constraint FK_dependre foreign key (idHopital)
      references Hopital (idHopital) on delete restrict on update restrict;

alter table Patient add constraint FK_cotiser foreign key (idMut)
      references Mutuelle (idMut) on delete restrict on update restrict;

alter table Patient add constraint FK_declarer_traitant foreign key (idMed)
      references Medecin (idMed) on delete restrict on update restrict;

alter table Sejour_Hospitalisation add constraint FK_aller foreign key (idPat)
      references Patient (idPat) on delete restrict on update restrict;

alter table Sejour_Hospitalisation add constraint FK_demander foreign key (idMed)
      references Medecin (idMed) on delete restrict on update restrict;

alter table Sejour_Hospitalisation add constraint FK_localiser foreign key (idSer)
      references Service (idSer) on delete restrict on update restrict;

alter table Service add constraint FK_diriger foreign key (idMed)
      references Medecin (idMed) on delete restrict on update restrict;

alter table Service add constraint FK_situer foreign key (idHopital)
      references Hopital (idHopital) on delete restrict on update restrict;

alter table exercer add constraint FK_exercer foreign key (idSer)
      references Service (idSer) on delete restrict on update restrict;

alter table exercer add constraint FK_exercer2 foreign key (idMed)
      references Medecin (idMed) on delete restrict on update restrict;

alter table guerir add constraint FK_guerir foreign key (idMaladie)
      references Maladie (idMaladie) on delete restrict on update restrict;

alter table guerir add constraint FK_guerir2 foreign key (idRem)
      references Remede (idRem) on delete restrict on update restrict;

alter table prescrireExam add constraint FK_prescrireExam foreign key (idConsult)
      references Consultation (idConsult) on delete restrict on update restrict;

alter table prescrireExam add constraint FK_prescrireExam2 foreign key (idTpExam)
      references TypeExamenMedical (idTpExam) on delete restrict on update restrict;

alter table prescrireMedoc add constraint FK_prescrireMedoc foreign key (idConsult)
      references Consultation (idConsult) on delete restrict on update restrict;

alter table prescrireMedoc add constraint FK_prescrireMedoc2 foreign key (idRem)
      references Remede (idRem) on delete restrict on update restrict;

alter table rembourser add constraint FK_rembourser foreign key (idRem)
      references Remede (idRem) on delete restrict on update restrict;

alter table rembourser add constraint FK_rembourser2 foreign key (idMut)
      references Mutuelle (idMut) on delete restrict on update restrict;

alter table reverser add constraint FK_reverser foreign key (idTpExam)
      references TypeExamenMedical (idTpExam) on delete restrict on update restrict;

alter table reverser add constraint FK_reverser2 foreign key (idMut)
      references Mutuelle (idMut) on delete restrict on update restrict;

