
ALTER TABLE Sejour_Hospitalisation
MODIFY dateSortie DATETIME NULL;


INSERT INTO `Sejour_Hospitalisation` (`idSejour`, `idPat`, `idMed`, `idSer`, `dateHAdmissiion`, `dateSortie`) VALUES
(2420, 27, 3316, 1634, '2019-12-12 12:12:12', '2019-12-15 12:12:12'),
(2421, 52, 3320, 1635, '2018-01-01 10:00:00', '2019-02-10 12:12:12'),
(2422, 28, 3310, 1638, '2019-04-01 09:00:00', '2019-04-10 10:00:00'),
(2423, 51, 3318, 1637, '2019-12-20 12:00:00', NULL),
(2424, 27, 3320, 1634, '2019-12-01 09:00:00', '2020-01-10 10:00:00'),
(2425, 28, 3310, 1638, '2019-12-01 10:00:00', '2019-01-02 10:00:00'),
(2426, 51, 3315, 1636, '2019-12-25 13:15:00', NULL),
(2427, 25, 3320, 1636, '2020-01-01 00:08:00', NULL),
(2428, 52, 3311, 1634, '2019-12-01 20:00:00', '2019-12-01 22:00:00');


INSERT INTO `Hopital` (`idHopital`, `nomHop`, `adHop`, `CPHop`, `villeHop`) VALUES
(2420, 'NECKER', '149 rue de Sèvre', '75015', 'PARIS'),
(2421, 'Americain de Reims', '47 Rue Cognack-Jay', '51000', 'REIMS'),
(2422, 'Americain de Paris', '63 Bd Victor Hugo', '92200', 'NEUILLY'),
(2423, 'Centre Hospitalier de Chaumont', '2 Rue Jeanne d\'Arc', '52000', 'CHAUMONT');


INSERT INTO `ExamenMedical` (`idExam`, `idTpExam`, `idPat`, `idLab`, `dateHExam`) VALUES
(2420, 1140, 26, 1622, '2019-12-12 10:05:32'),
(2421, 1141, 54, 1622, '2018-02-10 10:05:32'),
(2422, 1140, 2742, 1630, '2018-02-11 10:05:32'),
(2423, 1144, 27, 1622, '2019-12-12 10:05:32'),
(2424, 1143, 52, 1630, '2018-02-10 10:05:32'),
(2425, 1142, 26, 1623, '2018-02-11 10:05:32');




/*Injection de types d'examens*/

INSERT INTO TypeExamenMedical (idTpExam,nomTpExam) VALUES 
(2631,'Proctologie'),
(2632,'Cardiologie'),
(2633,'Scintigraphie'),
(2634,'Radiologie'),
(2635,'Neurologie'),
(2636,'Oncologie'),
(2637,'Psychiatrie'),
(2638,'Dialyse'),
(2639,'Maternité');

/*Injection de mutuelles*/

INSERT INTO Mutuelle (idMut,nomMut,adMut,CPMut,villeMut) VALUES 
(2631,'CCMO','12 rue Franck Ribery','51000','Reims'),
(2632,'MUT EST','10101 impasse du DIU','67000','Strasbourg'),
(2633,'ADREA','32 chemin Eric Galland','48000','Mendes'),
(2634,'GFP','7141 rue des Poêtes','38000','Grenoble'),
(2635,'Miltis','10 rue du Tigre','69000','Lyon'),
(2636,'Mutami','3640 impasse des Bretons','29000','Quimper'),
(2637,'MGEN','7 boulevard des Airs','06000','Nice'),
(2638,'MGC','29 avenue des Poules','07000','Aubenas'),
(2639,'SORUAL','45 rue Fabrice Servin','52190','Villiers-lès-Aprey');

/*Injection de reverser*/

INSERT INTO reverser (idTpExam, idMut, quotieTpExam) VALUES 
(2631, 2631, 0.85),
(2631, 2632, 0.95),
(2631, 2633, 0.65),
(2631, 2634, 0.75),
(2631, 2635, 0.55),
(2631, 2636, 0.45),
(2631, 2637, 0.35),
(2631, 2638, 0.75),
(2631, 2639, 0.75),
(2637, 2632, 0.68),
(2633, 2633, 0.69),
(2632, 2634, 0.79),
(2638, 2635, 0.56),
(2635, 2636, 0.49),
(2636, 2637, 0.37),
(2632, 2638, 0.73),
(2639, 2639, NULL);

/*Injection de Laboratoires*/

INSERT INTO Laboratoire (idLab, idHopital, nomLab, adLab, CPLab, villeLab) VALUES 
(2630, 1621, "Labobo", "43 boulevard Bouvard", "12080", "Villedieu" ),
(2631, NULL, "Bobolatête", "76 rue Binet", "72180", "Hvidovre" ),
(2632, 1624, "Labeauratoire", "4 avenue Nuenue", "13540", "Arles" ),
(2633, 1623, "Labo de Javel", "9 impasse Tonchemin", "25670", "Comté" ),
(2634, 1624, "Lavabo Ratoire", "87 avenue des Biles", "18000", "Rivière sur l'Eau" ),
(2635, 1622, "La Bora Toire", "5 rue Ru", "12090", "Sikert" ),
(2636, 1621, "Super Lab", "39 rue du Jura", "39000", "Lons" ),
(2637, 1621, "Doubs Labo", "37 rue de la Citadelle", "25000", "Besançon" ),
(2638, 1624, "Lab'haute Patate", "16 rue des pommes de terre", "70000", "Vesoul" ),
(2639, NULL, "Mon bô Labo", "43 rue du Quarante Trois", "43000", "Le Puy" );

/*Injection de patients*/

INSERT INTO Patient (idPat,idMut,idMed,nomPat,pnomPat,dateNais,adPat,CPPat,villePat) VALUES 
(22631,2632,3320,'TOURNOUX','Cédric',STR_TO_DATE('14/10/1975 13:40', '%j/%m/%Y %H:%i'),'34 rue des Escargots','52190','Villiers-lès-Aprey'),
(22632,2639,3315,'BOUABDILLAH','Oualid',STR_TO_DATE('11/04/1988 13:35', '%j/%m/%Y %H:%i'),'10 rue de la Gare','52200','Langres'),
(22633,2631,NULL,'CHIRAC','Jacques',STR_TO_DATE('28/11/1932 10:50', '%j/%m/%Y %H:%i'),'12 rue du Pere Lachaise','75000','Paris'),
(22634,2635,3310,'MACRON','Emmanuel',STR_TO_DATE('23/12/1977 08:50', '%j/%m/%Y %H:%i'),'69 rue Brigitte Lahaie','06969','Le Tube'),
(22635,2636,3319,'CHATEAU','Thierry',STR_TO_DATE('11/11/2011 11:11', '%j/%m/%Y %H:%i'),'1 impasse Partout','02400','Château Thierry'),
(22636,2633,NULL,'JACOB','Rabbi',STR_TO_DATE('12/12/2012 12:12', '%j/%m/%Y %H:%i'),'3 rue de la Gare','94800','Villejuif'),
(22637,2634,3315,'PAULJACQUES','Pierre',STR_TO_DATE('01/01/2001 01:01', '%j/%m/%Y %H:%i'),'158 avenue Pierre Pauljacques','69000','Lyon'),
(22638,2638,3314,'BELMONDO','Jean-Luc',STR_TO_DATE('02/02/2002 02:02', '%j/%m/%Y %H:%i'),'4 boulevard Jean-Paul Belmondo','62800','Liévin'),
(22639,2637,3313,'FRANCOIS','Alain',STR_TO_DATE('03/03/1978 19:58', '%j/%m/%Y %H:%i'),'10010 chemin du Binaire','51000','Reims');

/*Injection de Examen Médical*/

INSERT INTO ExamenMedical (idExam, idTpExam, idPat, idLab, dateHExam) VALUES 
(2631, 2632, 22633, 2634, STR_TO_DATE('02/01/2017 13:45', '%j/%m/%Y %H:%i')),
(2632, 2633, 22634, 2635, STR_TO_DATE('12/02/2017 15:45', '%j/%m/%Y %H:%i')),
(2633, 2634, 22635, 2636, STR_TO_DATE('22/03/2018 10:25', '%j/%m/%Y %H:%i')),
(2634, 2635, 22636, 2637, STR_TO_DATE('11/06/2018 15:00', '%j/%m/%Y %H:%i')),
(2635, 2636, 22637, 2638, STR_TO_DATE('10/07/2019 16:45', '%j/%m/%Y %H:%i')),
(2636, 2637, 22638, 2639, STR_TO_DATE('03/01/2019 13:45', '%j/%m/%Y %H:%i')),
(2637, 2638, 22639, 2635, STR_TO_DATE('02/01/2019 10:15', '%j/%m/%Y %H:%i')),
(2638, 2639, 22631, 2636, STR_TO_DATE('26/09/2019 17:45', '%j/%m/%Y %H:%i')),
(2639, 2631, 22632, 2637, STR_TO_DATE('02/10/2019 13:00', '%j/%m/%Y %H:%i'));


INSERT INTO `Sejour_Hospitalisation` (`idSejour`, `idPat`, `idMed`, `idSer`, `dateHAdmissiion`, `dateSortie`) VALUES
(2821, 2742, 3315, 1627, '2019-07-08 00:00:00', '2019-12-09 00:00:00'),
(2822, 2744, 3315, 1627, '2019-07-08 00:00:00', '2019-12-09 00:00:00'),
(2824, 2743, 3320, 1626, '2019-12-08 00:00:00', '2019-12-13 00:00:00'),
(2825, 2742, 3318, 1637, '2019-12-24 00:00:00', '2019-12-26 00:00:00'),
(2826, 2743, 3311, 1625, '2019-11-08 00:00:00', '2019-12-09 00:00:00'),
(2827, 2741, 3314, 1636, '2019-12-08 00:00:00', '2019-12-20 00:00:00'),
(2828, 2745, 3318, 1635, '2019-12-08 00:00:00', '2019-12-10 00:00:00'),
(2840, 2744, 3315, 1627, '2019-07-08 00:00:00', '2019-12-09 00:00:00'),
(2841, 2741, 3315, 1627, '2019-07-08 00:00:00', '2019-07-18 00:00:00'),
(2844, 2744, 3315, 1627, '2019-07-08 00:00:00', '2019-12-09 00:00:00'),
(2860, 2744, 3315, 1627, '2019-07-08 00:00:00', '2019-12-09 00:00:00'),
(2861, 2741, 3316, 1623, '2019-01-08 00:00:00', '2019-01-19 00:00:00'),
(2862, 2744, 3310, 1624, '2019-10-09 00:00:00', '2019-10-17 00:00:00'),
(2863, 2744, 3317, 1636, '2019-01-24 00:00:00', '2019-01-26 00:00:00'),
(2864, 52, 3310, 1630, '2019-02-08 00:00:00', '2019-03-09 00:00:00'),
(2865, 53, 3315, 1631, '2019-04-08 00:00:00', '2019-05-20 00:00:00'),
(2866, 27, 3316, 1634, '2019-01-08 00:00:00', '2019-11-10 00:00:00');




INSERT INTO `Consultation` (`idConsult`, `idMed`, `idPat`, `dateHConsult`) VALUES
(32820, 3310, 2741, '2013-03-19 11:30:00'),
(32821, 3311, 2742, '2014-04-19 11:30:00'),
(32822, 3312, 2743, '2014-04-20 11:30:00'),
(32823, 3314, 2743, '2012-02-17 11:30:00'),
(32824, 3310, 2741, '2013-09-23 11:30:00'),
(32825, 3311, 2741, '2013-11-23 11:45:00'),
(32826, 3317, 2745, '2015-02-24 09:45:00'),
(32827, 3318, 2744, '2015-03-25 08:45:00'),
(32828, 3318, 2743, '2015-04-26 08:45:00'),
(32829, 3319, 2742, '2016-05-27 10:45:00'),
(32830, 3320, 2741, '2017-06-28 14:45:00');


INSERT INTO `ExamenMedical` (`idExam`, `idTpExam`, `idPat`, `idLab`, `dateHExam`) VALUES
(2821, 1140, 2741, 1621, '2000-05-01 00:00:00'),
(2822, 1143, 2745, 1626, '2002-06-01 00:00:00'),
(2823, 1143, 52, 1623, '1999-06-01 00:00:00'),
(2824, 1144, 29, 1624, '2010-06-12 00:00:00'),
(2825, 1142, 27, 1621, '1999-01-30 00:00:00'),
(2826, 1141, 55, 1622, '1998-07-14 00:00:00'),
(2827, 1141, 26, 1622, '2019-06-07 00:00:00'),
(2828, 1142, 28, 1622, '1990-08-30 00:00:00'),
(2829, 1141, 2742, 1622, '1997-09-01 14:50:00'),
(2830, 1142, 2744, 1623, '2008-07-12 10:10:00'),
(2831, 1143, 2743, 1625, '2005-03-30 15:15:00');
