
INSERT INTO Remede (idRem,nomRem) VALUES
(1,"Aspirine") ,
(2,"Paracétamol") ,
(3,"Amoxiciline") ,
(4,"Supositoire Miam") ,
(5,"Viagra") ,
(6,"prostamole") ,
(7,"dragées Fuca"),
(8, "Vitamine C"),
(9,"Smecta"),
(10,"Whisky");


INSERT INTO Maladie VALUES (2741,"Grippe","SER");
INSERT INTO Maladie VALUES (2742,"Rhume","BEN");
INSERT INTO Maladie VALUES (2743,"Peste","CRI");
INSERT INTO Maladie VALUES (2744,"Gastro-entérite","SER");
INSERT INTO Maladie VALUES (2745,"Cancer","CRI");
INSERT INTO Maladie VALUES (2746,"Migraine","BEN");


INSERT INTO `Hopital` (`idHopital`, `nomHop`, `adHop`, `CPHop`, `villeHop`) VALUES ('1', 'CHU REIMS', '47 RUE COGNAC JAY', '51100', 'REIMS'); 
INSERT INTO `Hopital` (`idHopital`, `nomHop`, `adHop`, `CPHop`, `villeHop`) VALUES ('3', 'Clinique Sainte-Marguerite', '68 Grand Rue', '57680', 'Novéant-sur-Moselle');
INSERT INTO `Hopital` (`idHopital`, `nomHop`, `adHop`, `CPHop`, `villeHop`) VALUES ('4', 'Grand Hôpital de l\'Est Francilien', '2-4 Cours de la Gondoire', '77600', 'Jossigny');
INSERT INTO `Hopital` (`idHopital`, `nomHop`, `adHop`, `CPHop`, `villeHop`) VALUES ('2', 'GRAND GALOP', '18 RUE D\'OSEILLE', '51100', 'REIMS') ;
INSERT INTO `Hopital` (`idHopital`, `nomHop`, `adHop`, `CPHop`, `villeHop`) VALUES ('5', 'Centre Hospitalier de Toul Saint-Charles', '1 Cours Raymond Poincaré', '54201', 'Toul');


INSERT INTO Medecin (idMed, nomMed, pnomMed, adMed, CPMed, villeMed)
 VALUES
 (3310, 'Giraud', 'Frédérique', '10 rue de vignes', '51100', 'Reims'),
(3311, 'Rigault', 'Christian', '4 Allée Charles Baudelaire', '51450', 'Saint Memmie'),
(3312, 'Maudoigt', 'Valérie', '10 rue de vinerie', '51100', 'Reims'),
(3313, 'MBarek', 'Soraya', '154 av Jean Jaures', '51200', 'St Eupraise'),
(3314, 'Papillon', 'Catherine', '23 Bd Jean Giono', '84260', 'Sarrians'),
(3315, 'Russier', 'Yves', '2 rond point de Paris', '84200', 'Carpentras'),
(3316, 'Le flohic', 'Yann', '27 route de Theil', '61340', 'Berdhuis'),
(3317, 'Lalain', 'Jean Marc', '38 rue du pommier', '61200', 'Argentant'),
(3318, 'Sultant', 'Andrée', '111 rue Roger François', '94700', 'Maison alfort'),
(3319, 'Levis', 'Strauss', '91 rue artsitide Briand', '94230', 'Cachan'),
(3320, 'Haliday', 'Johnny', '6 AV de la Victoire', '94310', 'Orly');


INSERT INTO Hopital(idHopital, nomHop, adHop, CPHop, villeHop) VALUES
(1621,'Hopital de Reims','Rue trucmuche','51100','Reims'),
(1622,'Hopital de Lille','Rue machin','59000','Lille'),
(1623,'Hopital de Strasbourg','Rue tsointsoin','67000','Strasbourg'),
(1624,'Hopital de Dijon','Rue tralala','21000','Dijon');


INSERT INTO Service (idSer, idHopital, idMed, nomSer) VALUES       
(1621,1621,3310, 'Pédiatrie'),
(1622,1621,3311, 'Ophtalmologie'),
(1623,1621,3313, 'Gynécologie'),
(1624,1621,3313, 'Oncologie gynécologique'),
(1625,1622,3314, 'Pédiatrie'),
(1626,1622,3315, 'Ophtalmologie'),
(1627,1623,3317, 'Pédiatrie'),
(1628,1623,3318, 'Urologie'),
(1629,1624,3319, 'Neurochirurgie');



INSERT INTO Laboratoire VALUES
(1621,1621,'Labo alpha',NULL,NULL,'Reims'),
(1622,1622,'Labo beta',NULL,NULL,'Lille'),
(1623,1622,'Labo gamma',NULL,NULL,'Lille'),
(1624,1623,'Labo theta',NULL,NULL,'Strasbourg'),
(1625,1624,'Labo epsilon1',NULL,NULL,'Dijon'),
(1626,1624,'Labo epsilon2',NULL,NULL,'Dijon'),
(1627,1624,'Labo epsilon3',NULL,NULL,'Dijon');


INSERT INTO TypeExamenMedical (idTpExam, nomTpExam)
VALUES (1140, 'angiographie'),(1141, 'echographie'),(1142, 'irm'),(1143, 'scanner'),(1144, 'phlebographie');


INSERT INTO Mutuelle( idMut, nomMut, adMut, CPMut, villeMut) VALUES (5012, 'MGEN', 'rue des écus', '51100', 'REIMS');
INSERT INTO Mutuelle(idMut,nomMut,adMut,CPMut,villeMut)
    VALUES(19,"trucmuche","50 rue de la Fatigue","51100","Reims");
    
INSERT INTO `Mutuelle` (`idMut`, `nomMut`, `adMut`, `CPMut`, `villeMut`) VALUES
(16, 'mgenr', '17bis rue des ragondins', '62380', 'campeurville'),
(17, 'santeplus', 'impasse perdue', '51240', 'hardenay'),
(18, 'harmonie', '5 rue du pavé', '70120', 'troulaville');

INSERT INTO Mutuelle (idMut, nomMut, adMut, CPMut, villeMut)
	VALUES (10,"tous ensemble","2, rue Bidon",'51100','Reims');
INSERT INTO Mutuelle 
	VALUES (11,'Mutmut',"11, rue double sens",'75000','Paris');

INSERT INTO Mutuelle  
	VALUES (12, "Grand corps gueri","4, rue du Dr Bobo", '51100','Reims');
	
INSERT INTO Mutuelle  
	VALUES (13, "Ma Mutuelle","10, rue de la Providence", '22601',"Bobo Dioulasso"),
		(14, "Mutuelle générale","20, rue Bidon",'51100','Reims'),
		(15, "Mutuelle pour tous","78, avenue du revenu", '51100','Reims');
