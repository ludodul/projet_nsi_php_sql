
INSERT INTO Mutuelle
VALUES (2131, 'La Mutuelle', '2 rue de la Santé', '08000', 'Charleville-Mézières');

INSERT INTO Mutuelle
VALUES (2132, 'MGEN', '23 rue des Tambours', '08000', 'Charleville-Mézières');

INSERT INTO Mutuelle
VALUES (2133, 'Harmonie Mutuelle', '10 rue du fort', '08000', 'Charleville-Mézières');

INSERT INTO Mutuelle
VALUES (2134, 'MAAF', '14 rue du stade', '08200', 'Sedan');

INSERT INTO Mutuelle
VALUES (2135, 'CAFMUT', '23 avenue des martyrs de la résistance', '08200', 'Sedan');




INSERT INTO Patient (idPat, idMut, idMed, nomPat, pnomPat, dateNais, adPat, CPPat, villePat )
VALUES (2131, 2131, 3310, 'Rotta', 'Nicolas', STR_TO_DATE('27/06/1975', '%d/%m/%Y'),'5 ruelle des tulipes','51100','Reims');

INSERT INTO Patient
VALUES (2132, 2131, 3312, 'Boulet', 'Denis', STR_TO_DATE('12/11/1940', '%d/%m/%Y'),'5 allées des jonquilles','08200','Sedan');

INSERT INTO Patient
VALUES (2133, 2134, 3315, 'Lefevre', 'Alain', STR_TO_DATE('10/01/1984', '%d/%m/%Y'),'2, rue des toutous','08000','Charleville-Mézières');

INSERT INTO Patient
VALUES (2134, 2132, 3313, 'Debarge', 'Noel', STR_TO_DATE('27/06/1972', '%d/%m/%Y'),'15 rue des cochonnes','08400','Revin');

INSERT INTO Patient
VALUES (2135, 2133, 3316, 'renard', 'stéphane', STR_TO_DATE('27/05/1975', '%d/%m/%Y'),'105 rue de la rivière','08170','Fumay');

INSERT INTO Patient
VALUES (2136, 2132, 3317, 'carbot', 'johnny', STR_TO_DATE('24/08/1945', '%d/%m/%Y'),'70 avenue des oiseaux','08140','Donchery');

INSERT INTO Patient
VALUES (2139, 2134, 3318, 'Nette', 'Simone', STR_TO_DATE('13/12/1969', '%d/%m/%Y'),'25 avenue des roses','51100','Reims');



INSERT INTO Consultation
VALUES (2131, 3310, 2741, STR_TO_DATE('2012-02-17 11:30:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2132, 3320, 2745, STR_TO_DATE('2019-12-17 12:45:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2133, 3312, 2745, STR_TO_DATE('2017-01-12 10:00:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2134, 3318, 2742, STR_TO_DATE('2019-12-12 09:30:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2135, 3311, 2743, STR_TO_DATE('2019-11-25 17:34:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2136, 3318, 2741, STR_TO_DATE('2018-10-05 14:00:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2137, 3317, 2743, STR_TO_DATE('2015-03-21 07:40:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2138, 3312, 2745, STR_TO_DATE('2017-01-12 10:00:00', '%Y-%m-%d %H:%i:%s'));

INSERT INTO Consultation
VALUES (2139, 3318, 2742, STR_TO_DATE('2019-12-12 09:30:00', '%Y-%m-%d %H:%i:%s'));



INSERT INTO prescrireExam ( idConsult, idTpExam)
VALUES (1, 1140);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (2, 1140);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (6, 1142);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (5, 1140);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (10, 1144);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (9, 1144);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (3, 1141);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (3, 1143);

INSERT INTO prescrireExam( idConsult, idTpExam)
VALUES (7, 1142);




INSERT INTO Laboratoire (idLab, idHopital, nomLab, adLab,CPlab, villeLab) VALUES
(2331, 1, "Labo 1", "1 rue du fond", "10000", "Troyes"),
(2332, 2, "Labo 2", "8 avenue de nulle part", "51100", "Reims"),
(2333, 3, "Labo 3", "61 boulevard du DIU", "08000", "Charleville-Mézières"),
(2334, 1621, "Labo 4", "impasse politique", "08090", "Trou du...Monde"),
(2335, 1622, "Labo 5", "7 rue de sans issue", "59000", "Lille"),
(2336, 1623, "Labo 6",  "10 avenue du vase", "02200", "Soissons"),
(2337, 1624, "Labo 7", "5 impasse du paradis", "75000", "Paris"),
(2338, 2, "Labo 8", "6 planète Mars", "51000","Châlon en Champagne");

INSERT INTO Mutuelle (idMut, nomMut, adMut, CPMut, villeMut) VALUES
(2331,"Mut 1","3 rue de la gare","54390","Frouard"),
(2332,"Mut 2","2 rue du fort","77000","Meaux"),
(2333,"Mut 3","15 rue du moulin","10000","Troyes"),
(2334,"Mut 4","7 rue du jardin","54000","Nancy"),
(2335,"Mut 5","25 rue de l'école","08000","Charleville-Mézière"),
(2336,"Mut 6","14 rue de l'église","75000","Paris"),
(2337,"Mut 7"," 34 rue de Verdun","51000","Reims");

INSERT INTO TypeExamenMedical(idTpExam,nomTpExam) VALUES
(2331,"Torture mentale"),
(2332,"Triturage"),
(2333,"Enfonçage"),
(2334,"Tabassement"),
(2335,"Visseuse électrique"),
(2336,"Aaaahhhrggg");

insert into ExamenMedical(idExam,idTpExam,idPat,idLab,dateHExam) values
(2331,2331,25,2331,STR_TO_DATE('01/02/2020 14:30','%d/%m/%Y %H:%i')),
(2332,2332,26,2332,STR_TO_DATE('05/09/2020 08:55','%d/%m/%Y %H:%i')),
(2333,2333,27,2333,STR_TO_DATE('30/12/2019 10:20','%d/%m/%Y %H:%i')),
(2334,2334,28,2334,STR_TO_DATE('25/12/2019 00:00','%d/%m/%Y %H:%i')),
(2335,2335,29,2335,STR_TO_DATE('17/01/2020 12:45','%d/%m/%Y %H:%i')),
(2336,2336,51,2336,STR_TO_DATE('05/05/2020 19:00','%d/%m/%Y %H:%i'));




