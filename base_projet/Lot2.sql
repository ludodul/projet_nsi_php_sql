
INSERT INTO guerir (idMaladie,IdRem) VALUES
(2741,1),
(2742,4),
(2743,5),
(2744,6),
(2745,8),
(2746,7),
(2741,8),
(2746,10);



INSERT INTO exercer (idSer, idMed)
 VALUES
 (1621, 3310),
 (1622, 3311),
 (1623, 3313),
 (1624, 3313),
 (1625, 3314),
 (1626, 3315),
 (1627, 3317),
 (1628, 3318),
 (1629, 3319),
 (1626, 3313),
 (1624, 3314),
 (1621, 3313),
 (1622, 3317),
 (1625, 3313);


INSERT INTO Patient(idPat, idMut, idMed, nomPat, pnomPat, dateNais,adPat,CPPat,villePat) VALUES
(2741,17,3310,'DUSCHMOL','Ginette',STR_TO_DATE('18/07/1964','%d/%m/%Y'),'3 Rue des Graviers','51100','REIMS');
INSERT INTO Patient VALUES
(2742,17,3310,"DUSCHMOL","Raymond",STR_TO_DATE("15/11/1962","%d/%m/%Y"),"3 Rue des Graviers","51100","REIMS"),
(2743,18,3313,"MARTIN","Eric",STR_TO_DATE("25/09/1972","%d/%m/%Y"),"43 Avenue des Alouettes","10000","TROYES"),
(2744,19,3313,"MARTIN","Josiane",STR_TO_DATE("01/04/1976","%d/%m/%Y"),"6 Allée des Jardins","51450","BETHENY"),
(2745,5012,NULL,"DUPONT","Téo",STR_TO_DATE("16/12/2001","%d/%m/%Y"),"18 Boulevard Robespierre","10100","ROMILLY SUR SEINE");

INSERT INTO Patient(idPat,idMut,idMed,nomPat,pnomPat,dateNais,adPat,CPPat,villePat) VALUES
	(51,10,3319,"Dugenou","Jean",STR_TO_DATE('01-05-2002', '%d-%m-%Y'),"56, rue du pied",'51000','Reims'),
	(52,15,3316,"El Amin","Arthur",STR_TO_DATE('27-09-1987', '%d-%m-%Y'),"48, avenue de la paix",'51000','Reims'),
	(53,14,3317,"Coco","Alain",STR_TO_DATE('24-01-1959', '%d-%m-%Y'),"8, rue d'enfer",'51300',"Vitry le Francois"),
	(54,14,3317,"Cherout","Cherif",STR_TO_DATE('12-03-1996', '%d-%m-%Y'),"8, rue d'enfer",'51300',"Vitry le Francois"),
	(55,13,3315,"Surin","Farid",STR_TO_DATE('12-09-2015', '%d-%m-%Y'),"45, rue du puits",'75000',"Paris");

INSERT INTO `reverser` (`idTpExam`, `idMut`, `quotieTpExam`) 
VALUES (1140, 10, 0.70), (1141, 11, 0.6), (1142, 14, 0.8), (1143, 13, 0.5), (1144, 16, 0.8);

INSERT INTO Patient(idPat,idMut,idMed,nomPat,pnomPat,dateNais,adPat,CPPat,villePat) VALUES
    (25,11,3312,"Ibulaire","Pat",STR_TO_DATE('02-12-1937','%d-%m-%Y'),"13 rue du Bidule",'51100','Reims'),
    (26,13,3312,"Aymar","Jean",STR_TO_DATE('05-10-1997','%d-%m-%Y'),"34 rue du Machin","51423","Vacances"),
    (27,11,3319,"Bon","Jean",STR_TO_DATE('24-05-1957','%d-%m-%Y'),"73 rue du Machinchose","51100","Reims"),
    (28,11,3312,"Noel","Pierre",STR_TO_DATE('17-03-1974','%d-%m-%Y'),"22 avenue du Machin","51423","Vacances"),
    (29,15,3314,"Mama","Mimi",STR_TO_DATE('02-12-1937','%d-%m-%Y'),"13 rue du Oula","51100","Reims");
	
	
INSERT INTO rembourser (idRem, idMut, quotiteRem )
 VALUES
 (1, 10, 0.95),
 (1, 11, 0.95),
 (1, 12, 0.95),
 (1, 13, 0.95),
 (1, 14, 1.00),
 (1, 17, 1.00),
 (1, 5012, 0.50),
 (2, 15, 0.95),
 (2, 16, 0.95),
 (2, 17, 0.95),
 (2, 18, 0.95),
 (2, 19, 0.95),
 (2, 5012, 0.85),
 (2, 14, 0.85),
 (2, 13, 0.80),
 (3, 11, 1.00),
 (3, 12, 1.00),
 (3, 13, 0.85),
 (3, 10, 1.00),
 (3, 14, 0.85),
 (3, 15, 1.00),
 (3, 16, 0.85),
 (3, 17, 1.00),
 (3, 18, 0.85),
 (3, 19, 0.85),
 (4, 17, 1.10),
 (4, 19, 0.8),
 (4, 15, 0.50),
 (5, 13, 0.20),
 (5, 14, 0.20),
 (5, 16, 0.20),
 (5, 17, 0.00),
 (5, 18, 0.00),
 (6, 12, 0.50),
 (6, 13, 0.50),
 (6, 15, 0.50),
 (6, 16, 0.50),
 (6, 14, 0.65),
 (6, 17, 0.65),
 (6, 18, 0.85),
 (7, 15, 0.85),
 (7, 16, 0.85),
 (7, 17, 0.85),
 (7, 18, 0.85),
 (7, 5012, 0.85),
 (8, 10, 0.85),
 (8, 11, 1.00),
 (8, 12, 1.00),
 (8, 13, 1.00),
 (8, 14, 1.00),
 (8, 15, 1.00),
 (8, 16, 1.00),
 (8, 18, 0.50),
 (8, 19, 0.50),
 (9, 11, 0.40),
 (9, 12, 0.40),
 (9, 13, 0.40),
 (9, 14, 0.40),
 (9, 15, 0.40),
 (9, 16, 0.40),
 (10, 10, 1.00),
 (10, 11, 0.20),
 (10, 12, 0.00),
 (10, 13, 0.50),
 (10, 14, 0.62),
 (10, 15, 0.70),
 (10, 16, 0.30),
 (10, 17, 2.00),
 (10, 18, 0.25),
 (10, 19, 0.85),
 (10, 5012, 0.05);

 


