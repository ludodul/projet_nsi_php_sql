## Projet de base de données avec SQL et PHP

### Installation de php ubuntu:

```bash
sudo apt install php-cli
```
### Guide d'installation de phpmyadmin sur ubuntu:
https://guide.ubuntu-fr.org/server/phpmyadmin.html

### Enoncé du projet:

[ProjetBDD.pdf](ProjetBDD.pdf)

### Lien vers les ressources de Jérôme CURTONA
https://iut-info.univ-reims.fr/users/cutrona/intranet/DIU.html


## Installation de la base

Ouvrir un terminal à la racine du projet.  

```bash
mysql -u root -p
```
password : root

```sql
DROP DATABASE sante;
CREATE DATABASE sante;
exit
```

```bash
mysql -u root -p sante < base_projet/crebas_SanteMySQL.sql
mysql -u root -p sante < base_projet/Lot1.sql
mysql -u root -p sante < base_projet/Lot2.sql
mysql -u root -p sante < base_projet/Lot3.sql
mysql -u root -p sante < base_projet/Lot4.sql
mysql -u root -p sante < base_projet/Lot5.sql
mysql -u root -p sante < base_projet/Lot6.sql


password : root 
```

### Si php est installé, lnacer un serveur php:
```bash
php -S localhost:8080
```

puis naviguer sur l'url: http://localhost:8080/php/index.php


