<?php
$page = <<<HTML
<!doctype html>
<html lang = 'fr'>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="$Autor">
        <meta name="generator" content="VSCode">
        <title>$title</title>
        <link rel="icon" href="../images/logo.png">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script>
          $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
          });
        </script>
        
    </head>
<body class="bg-gray-100 pt-16">
  
<nav class="navbar navbar-expand bg-dark navbar-dark mb-4 shadow fixed-top">
  <a class="navbar-brand" href="#">
    <img src="../images/logo.png" alt="Logo" style="width:40px;">
  </a>
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.php">Accueil</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="rechercher.php">Rechercher</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="inserer.php">Inserer</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete1.php">Requete1</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete2.php">Requete2</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete3.php">Requete3</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete4.php">Requete4</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete5.php">Requete5</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete6.php">Requete6</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete7.php">Requete7</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="requete8.php">Requete8</a>
    </li>  
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Liens Utiles
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a href="https://iut-info.univ-reims.fr/users/cutrona/intranet/DIU.html" class="dropdown-item" target="_blank">Ressources php</a>
          <a href="https://gitlab.com/ludodul/projet_nsi_php_sql" class="dropdown-item" target="_blank">projet Gitlab</a>
        </div>
    </li>
  </ul>
</nav>
<div  style="margin: 20px">
HTML;
echo $page;