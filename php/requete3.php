<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete3";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
$pdo = $Requete->connect();

$req = <<<SQL
SELECT  CONCAT(pnomPat," ",UPPER(nomPat)) "Patient",
        CONCAT(adPat," ",CPPat," ",villePat) "Adresse",
        TRUNCATE((DATE(NOW())-DATE(dateNais))/10000,0) "Age",
        CASE WHEN idMed IS NULL THEN "non" ELSE "oui" END "Medecin traitant"
FROM Patient
WHERE CPPat = :CP AND 
    ((DATE(NOW())-DATE(dateNais)>560000)
    AND (DATE(NOW())-DATE(dateNais)<800000)
    OR idMed IS NULL)
ORDER BY nomPat ;
SQL;

try {
    $pdoStat = $pdo->prepare($req);
    $pdoStat->bindValue("CP","51100");
    $pdoStat->execute();

    $page =<<<HTML

    <h1>Requête simple, mais un peu moins...</h1>
    <p>
        Donner la liste des patients ayant 51100 comme code postal,
        qui ont entre 56 et 80 ans ou bien qui n'ont pas déclaré de médecin traitant,
        triée par ordre alphabétique des noms,
        sous la forme prénom-nom, adresse.
    </p>
    <h2>La requête</h2>
    <div class="shadow-md rounded-lg" style="width:620px;height:240px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT CONCAT(pnomPat," ",UPPER(nomPat)) "Patient",
    CONCAT(adPat," ",CPPat," ",villePat) "Adresse",
    TRUNCATE((DATE(NOW())-DATE(dateNais))/10000,0) "Age",
    CASE WHEN idMed IS NULL THEN "non" ELSE "oui" END "Medecin traitant"
FROM Patient
WHERE CPPat = 51100 AND 
    ((DATE(NOW())-DATE(dateNais)>560000)
    AND (DATE(NOW())-DATE(dateNais)&lt800000)
    OR idMed IS NULL)
ORDER BY nomPat ;
        </pre>

    </div>
    <div>
        <h2>Les résultats</h2>
        <table class="table table-striped table-bordered" style="width:800px;">
            <tr>
                <th>Nom</th>
                <th>Adresse</th>
                <th>Age</th>
                <th>Médecin traitant</th>
            </tr>

HTML;

    while($ligne = $pdoStat->fetch(PDO::FETCH_NUM)) {
        $page.="            <tr>\n";
        $page.="                <td>$ligne[0]</td>\n";
        $page.="                <td>$ligne[1]</td>\n";
        $page.="                <td>$ligne[2]</td>\n";
        $page.="                <td>$ligne[3]</td>\n";
        $page.="            </tr>\n";        
    }

    $page .=<<<HTML
        </table>
    </div>
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
} 
catch (Exception $e) {
    echo "<p>ERREUR :".$e->getMessage()."</p>";
}