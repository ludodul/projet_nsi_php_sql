<?php 


$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Insertion";
require 'header.php';

require_once '../class/Requetes.class.php';
$Requete = new Requetes;



if (isset($_GET['key1']))
{
  $key_get = htmlentities($_GET['key1']);
  $first = $Requete->getTable()[$key_get];
}else{
$first = "Choisissez...";
};
$page = <<<HTML

<form action = "inserer.php" method="GET">
  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Tables</label>
    </div>
    <select class="custom-select" id="inputGroupSelect01" name="key1" onchange="this.form.submit()">
      <option selected>$first</option>
      <option value="1">Sejour Hospitalisation</option>
      <option value="2">Maladie</option>
      <option value="3">Remede</option>
      <option value="4">Guerir</option>
      <option value="5">Rembourser</option>
      <option value="6">Mutuelle</option>
      <option value="7">Patient</option>
      <option value="8">Consultation</option>
      <option value="9">Laboratoire</option>
      <option value="10">Hopital</option>
      <option value="11">Type Examen Médical</option>
      <option value="12">Service</option>

    </select>
  </div>
</form>

HTML;

if (isset($_GET['key1']))
{       
    
    $res = $Requete->getKeys($key_get);
    
;    $table_name = $res['table_name'];
    $page .= <<<HTML
    
    <form action = "inserer.php" method="POST"> 
      <input type="hidden" name ="table_name" value="$table_name">
      <table class="table table-striped table-bordered" style="width:700px;">
        <tr>
          <th>Nom</th>
          <th>type</th>
          <th>insérer</th>
          <th>Détail</th>
        </tr>

HTML;

    $champs = array();

    foreach($res['nofkeys'] as $line){
        
        $name=$line['name'];
        
        if (!in_array($name,$champs)){
          $champs[] = $name;
          
          $dataType = $line['dataType'];
          $detail = $line['detail'];
          
          if ($name != "graviteMal") {
            $page .=<<<HTML
        <tr>
          <td>$name</td>
          <td>$dataType</td>
          <td>  
            <input class="form-control" name="$name" required>
          </td>
          <td>$detail</td>
                  
        </tr>
    


HTML;
        } else {
            $page .=<<<HTML
      <tr>
          <td>$name</td>
          <td>Automatique</td>
          <td>  
    
            <div class="form-group">
              <select class="form-control" name="$name" value="" required>
                <option disabled selected value> -- Choisissez -- </option>
                <option class="form-control" value="BEN" name="$name">BEN</option>
                <option class="form-control" value="SER" name="$name">SER</option>
                <option class="form-control" value="CRI" name="$name">CRI</option>
              </select>
            </div>
          </td>
      </tr>
              
HTML;
        }
      }
};

    foreach($res['fkeys'] as $id => $table){
      
      $res_values = $Requete->resValues($id, $table);
      
      $page .=<<<HTML

      <tr>
          <td>$id</td>
          <td>Automatique</td>
          <td>  
    
            <div class="form-group">
              <select class="form-control" id="$table" name="$id" value="" required>
                <option disabled selected value> -- Choisissez -- </option>
              
HTML;
      $page .="\n";
      foreach($res_values as $index=>$value)
      {
        $ide = $value['id'];
        $nom = $value['nom']." ($ide)";
        
        $page.=<<<HTML
                <option class="form-control" value="$ide" name="$ide">$nom</option>
HTML;
        $page .="\n";
      } 
        $page.=<<<HTML
              </select>
            </div>
          </td>
          <td></td>
      </tr>
HTML;



};
$page .="\n";
$page .= <<<HTML
  </table>
  <button class="btn btn-secondary" type="submit" >Insérer</button>
</form>
HTML;
$page .="\n";

};

if (isset($_POST['table_name']))
{
  $_POST = array_map('htmlspecialchars', $_POST);
  $Requete->insert($_POST);
}
if (isset($_GET['success']))
{
  if (htmlentities($_GET['success'])==='success')
  {
    $page .= <<<HTML
    <div class="alert alert-success">
      <strong>Succès!</strong> Insertion Réussie!
    </div>
HTML;
  }
}




$page .= <<<HTML
</div>
</body>
</html>
HTML;

echo $page;
