<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete7";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
$pdo = $Requete->connect();

$req = <<<SQL
SELECT LOWER(rem.nomRem) "Remède",
       IFNULL (UPPER(m.nomMut), "Aucune mutuelle") "Mutuelle"
FROM Remede rem 
     LEFT JOIN rembourser rbt ON rem.idRem=rbt.idRem
     LEFT JOIN Mutuelle m ON rbt.idMut=m.idMut
ORDER BY 1;
SQL;

try {
    $query = $pdo->query($req);    
    $page =<<<HTML

    <h1>Une requête avec jointure externe</h1>
    <p>
        La liste de tous les remèdes, triée par ordre alphabétique,
        et, pour chacun d'eux, la liste, éventuellement vide,
        des mutuelles qui le remboursent.
    </p>
    <h2>La requête</h2>
    <div class="shadow-md rounded-lg" style="width:530px;height:140px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT LOWER(rem.nomRem) "Remède",
       IFNULL (UPPER(m.nomMut), "Aucune mutuelle") "Mutuelle"
FROM Remede rem 
     LEFT JOIN rembourser rbt ON rem.idRem=rbt.idRem
     LEFT JOIN Mutuelle m ON rbt.idMut=m.idMut
ORDER BY 1;
        </pre>

    </div>
    <div>
        <h2>Les résultats</h2>
        <table class="table table-striped table-bordered" style="width:400px;">
            <tr>
                <th>Remède</th>
                <th>Mutuelle</th>
                                
            </tr>

HTML;

    while($ligne = $query->fetch(PDO::FETCH_NUM)) {
        $page.="            <tr>\n";
        $page.="                <td>$ligne[0]</td>\n";
        $page.="                <td>$ligne[1]</td>\n";
        $page.="            </tr>\n";        
    }

    $page .=<<<HTML
        </table>
    </div>
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
} 
catch (Exception $e) {
    echo "<p>ERREUR :".$e->getMessage()."</p>";
}