<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete6";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
$pdo = $Requete->connect();

$req = <<<SQL
SELECT DISTINCT rem.NomRem "Nom du remède"
FROM Remede rem 
WHERE rem.IdRem IN (SELECT rbt.IdRem
                    FROM rembourser rbt)
ORDER BY 1;
SQL;

try {
    $query = $pdo->query($req);    
    $page =<<<HTML

    <h1>Requête avec sous requête utilisant IN</h1>
    <p>
        La liste des remèdes remboursés par au moins une mutuelle, avec leur nom,
        triés par ordre alphabétique
    </p>
    <h2>La requête</h2>
    <div class="shadow-md rounded-lg" style="width:380px;height:120px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT DISTINCT rem.NomRem "Nom du remède"
FROM Remede rem 
WHERE rem.IdRem IN (SELECT rbt.IdRem
                    FROM rembourser rbt)
ORDER BY 1;
        </pre>

    </div>
    <div>
        <h2>Les résultats</h2>
        <table class="table table-striped table-bordered" style="width:200px;">
            <tr>
                <th>Nom du remède</th>
                               
            </tr>

HTML;

    while($ligne = $query->fetch(PDO::FETCH_NUM)) {
        $page.="            <tr>\n";
        $page.="                <td>$ligne[0]</td>\n";
        $page.="            </tr>\n";        
    }

    $page .=<<<HTML
        </table>
    </div>
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
} 
catch (Exception $e) {
    echo "<p>ERREUR :".$e->getMessage()."</p>";
}