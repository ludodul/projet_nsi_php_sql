<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete4";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
$pdo = $Requete->connect();

$req = <<<SQL
SELECT DATE_FORMAT(dateHConsult,"le %d/%m/%Y à %H:%i") "Date et heure",
       CONCAT(UPPER(nomMed)," ",pnomMed) "Médecin"

FROM Consultation c
    INNER JOIN Patient p ON c.idPat=p.idPat
    INNER JOIN Medecin m ON c.idMed=m.idMed
WHERE (UPPER(nomPat)="IBULAIRE" AND UPPER(pnomPat)="PAT")
ORDER BY 1;
SQL;

try {
    $query = $pdo->query($req);    
    $page =<<<HTML

    <h1>Requête avec jointure interne</h1>
    <p>
        Donner la liste des consultations de Pat Ibulaire, triée par date
    </p>
    <h2>La requête</h2>
    <div class="shadow-md rounded-lg" style="width:620px;height:160px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT DATE_FORMAT(dateHConsult,"le %d/%m/%Y à %H:%i") "Date et heure",
    CONCAT(UPPER(nomMed)," ",pnomMed) "Médecin"
FROM Consultation c
    INNER JOIN Patient p ON c.idPat=p.idPat
    INNER JOIN Medecin m ON c.idMed=m.idMed
WHERE (UPPER(nomPat)="IBULAIRE" AND UPPER(pnomPat)="PAT")
ORDER BY 1;
        </pre>

    </div>
    <div>
        <h2>Les résultats</h2>
        <table class="table table-striped table-bordered" style="width:500px;">
            <tr>
                <th>Date et Heure</th>
                <th>Médecin</th>                
            </tr>

HTML;

    while($ligne = $query->fetch(PDO::FETCH_NUM)) {
        $page.="            <tr>\n";
        $page.="                <td>$ligne[0]</td>\n";
        $page.="                <td>$ligne[1]</td>\n";
        $page.="            </tr>\n";        
    }

    $page .=<<<HTML
        </table>
    </div>
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
} 
catch (Exception $e) {
    echo "<p>ERREUR :".$e->getMessage()."</p>";
}