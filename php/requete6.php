<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete6";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
$pdo = $Requete->connect();

$req = <<<SQL
SELECT r.nomRem "Nom du remède soignant la grippe"
FROM Remede r
WHERE r.idRem IN (SELECT g.idRem
                   FROM guerir g
                   WHERE g.idMaladie = (SELECT m.idMaladie
                                        FROM Maladie m
                                        WHERE m.nomMal ="Grippe"))
ORDER BY 1;
SQL;

try {
    $query = $pdo->query($req);    
    $page =<<<HTML

    <h1>Une requête avec sous requête utilisant le = et le IN</h1>
    <p>
        La liste de tous les remèdes soignant la grippe triée par ordre alphabétique
    </p>
    <h2>La requête</h2>
    <div class="shadow-md rounded-lg" style="width:580px;height:175px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT r.nomRem "Nom du remède soignant la grippe"
FROM Remede r
WHERE r.idRem IN (SELECT g.idRem
                   FROM guerir g
                   WHERE g.idMaladie = (SELECT m.idMaladie
                                        FROM Maladie m
                                        WHERE m.nomMal ="Grippe"))
ORDER BY 1;
        </pre>

    </div>
    <div>
        <h2>Les résultats</h2>
        <table class="table table-striped table-bordered" style="width:300px;">
            <tr>
                <th>Nom de remède soignant la grippe</th>
                                
            </tr>

HTML;

    while($ligne = $query->fetch(PDO::FETCH_NUM)) {
        $page.="            <tr>\n";
        $page.="                <td>$ligne[0]</td>\n";
        $page.="            </tr>\n";        
    }

    $page .=<<<HTML
        </table>
    </div>
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
} 
catch (Exception $e) {
    echo "<p>ERREUR :".$e->getMessage()."</p>";
}