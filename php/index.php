<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Accueil";
require 'header.php';

$rand= random_int ( 1 , 8 );
$link = "requete"."$rand".".php";

$page = <<<HTML
		<h1 style="text-align:center">Gestion des données hospitalières</h1>
		<div class="container">
			<div class="d-flex row justify-content-center align-items-center " >
				<div class="zoom col-4-md col-12-sm align-self-stretch " style="margin: auto;">
					<a href="rechercher.php" data-toggle="tooltip" title="rechercher">
						<img  src="../images/hopital.png"  alt="Hopital" style="width: 100%; max-width: 300px; max-height: 300px;" >
					</a>
				</div>
				<div class="zoom col-4-md col-12-sm align-self-stretch  my-2 " style="margin: auto">
					<a href="$link" data-toggle="tooltip" title="requête aléatoire">
						<img class= "rounded-lg " src="../images/database.png"  alt="database" style="width: 100%; max-width: 300px; max-height: 300px;" >
					</a>
				</div>
				<div class="zoom col-4-md col-12-sm align-self-stretch " style="margin: auto;">
					<a href="inserer.php" data-toggle="tooltip" title="insérer">
						<img src="../images/database2.png"  alt="Hopital" style="width: 100%; max-width: 300px; max-height: 300px;" >
					</a>
				</div>
			</div>		
			<div class="d-flex row justify-content-center align-items-center;" >				
				<div class="col-6-md col-12-sm" style="margin: auto;" >
					<h2 style="text-align:center">Hopitaux de Reims</h2>
					<p> Cette interface permet d'interroger les tables des bases de données hospitalières.</p>
					<p> Plusieurs types de requêtes, avec réponses, sont aussi proposées.</p>
				</div>
				<div class="col-6-md col-12-sm" style="margin: auto;" >
					<h2 style="text-align:center">Annonces officielles</h2>
					<p>Projet PHP du <abbr title="Enseigner l'informatique au lycée">DIU NSI</abbr>.</p>
					<p>Groupe composé de Ludovic Tison, Samuel Pate et Nicolas Dauchy</p>
					
				</div>
			</div>
			<div class="d-flex row justify-content-center align-items-center;" >
				<a href="https://gitlab.com/ludodul/projet_nsi_php_sql" data-toggle="tooltip" title="gitlab" target="_blank">
					<img src="../images/gitlab.png"  alt="gitlab" style="width: 100%; max-width: 300px; max-height: 300px;" >
				</a>
				<a href="https://iut-info.univ-reims.fr/users/cutrona/intranet/DIU.html" target="_blank" data-toggle="tooltip" title="Jerome Cutrona">
					<img src="../images/jerome.png"  alt="gitlab" style="width: 100%; max-width: 300px; max-height: 300px;" >
				</a>
			</div>
			
		</div>
	</div>	
</body>
</html>
HTML;

echo $page;