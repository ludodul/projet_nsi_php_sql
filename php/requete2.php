<?php
header("Content-Type: text/html;charset=UTF-8");
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete2";
require 'header.php';
require_once '../class/Requetes.class.php';


if (isset($_GET['cp']))
{
    $CP = htmlentities($_GET["cp"]);
    $Requete = new Requetes;
    $pdo = $Requete->connect();
    $req = <<<SQL
    SELECT  CONCAT(pnomPat," ",UPPER(nomPat)) "Patient",
            adPat "Adresse",
            CONCAT(CPPat," ",UPPER(villePat)) "Ville"
    FROM Patient
    WHERE CPPat = :CP
    ORDER BY nomPat ;
SQL;
    $pdoStat = $pdo->prepare($req);
    $pdoStat->bindValue("CP","$CP");
    $pdoStat->execute();


    if ($ligne = $pdoStat->fetch(PDO::FETCH_NUM)) {
        
        $page = <<<HTML

    <h1>Requête</h1>
    <div class="shadow-md rounded-lg" style="width:450px;height:145px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT  CONCAT(pnomPat," ",UPPER(nomPat)) "Patient",
        adPat "Adresse",
        CONCAT(CPPat," ",UPPER(villePat)) "Ville"
FROM Patient
WHERE CPPat = $CP
ORDER BY nomPat;
        </pre>
    </div>

HTML;
        $page .="   <h1>Résultat</h1>\n   <p>Les patients dont le code postal est $CP sont :</p>\n";
        $page .= <<<HTML
    <table class="table table-striped table-bordered" style="width:800px;">
        <tr>
            <th>Nom</th>
            <th>Adresse</th>
            <th>Ville</th>
        </tr>

HTML;

        $page.="        <tr>\n";
        $page.="            <td>$ligne[0]</td>\n";
        $page.="            <td>$ligne[1]</td>\n";
        $page.="            <td>$ligne[2]</td>\n";
        $page.="        </tr>\n";

        while($ligne = $pdoStat->fetch(PDO::FETCH_NUM)) {
            $page.="        <tr>\n";
            $page.="            <td>$ligne[0]</td>\n";
            $page.="            <td>$ligne[1]</td>\n";
            $page.="            <td>$ligne[2]</td>\n";
            $page.="        </tr>\n";        
        }
    $page .= "    </table>\n";
    } else {
        $page = <<<HTML

    <h1>Requête</h1>
    <div style="border:solid 2px;width:450px;height:145px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT  CONCAT(pnomPat," ",UPPER(nomPat)) "Patient",
        adPat "Adresse",
        CONCAT(CPPat," ",UPPER(villePat)) "Ville"
FROM patient
WHERE CPPat = $CP
ORDER BY nomPat;
        </pre>
    </div>

HTML;
        $page .= "   <h1>Résultat</h1>\n   <p>Il n'y a aucun patient dont le code postal est $CP.</p>\n";
    }
    $page .= <<<HTML
    <input type="button" onclick="window.location.href='requete2.php'" value="Nouvelle recherche"/>
HTML;
}
else 
{
    $page = <<<HTML
    <h1>Requête simple avec paramètre</h1>
    <p>
        Entrez une code postal et cliquer sur le bouton,
        vous obtiendrez la liste des patients correspondant à ce code postal.
    </p>
    <form action="requete2.php" method="GET">
        <div style="width:500px">
            Entrer un code postal :
                <input class="col-6 form-control " type="text" name="cp" id="cp" placeholder="code postal" required></p>
        </div>
        <button class="btn btn-secondary" type="submit" value="Lancer la requête">Lancer la requête</button>
    </form>
HTML;
}


$page .=<<<HTML
        
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
 
