<?php
header("Content-Type: text/html;charset=UTF-8");
$Autor = "TISON-PATE-DAUCHY";
$title = "Rechercher";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
if (isset($_GET['idHop']))
{
    $idHop = htmlentities($_GET["idHop"]);

    $pdo = $Requete->connect();
    $req = <<<SQL
    SELECT nomHop
    FROM Hopital 
    WHERE idHopital=:idHop
SQL;
    $pdoStat = $pdo->prepare($req);
    $pdoStat->bindValue("idHop","$idHop");
    $pdoStat->execute();

    $nomHop = $pdoStat->fetch(PDO::FETCH_NUM)[0] ;
    
    $pdo = $Requete->connect();
    $req = <<<SQL
    SELECT nomMed,pnomMed,villeMed
    FROM Medecin 
    WHERE idMed IN (
        SELECT idMed 
        FROM Service 
        WHERE idHopital=:idHop
        )
SQL;
    $pdoStat = $pdo->prepare($req);
    $pdoStat->bindValue("idHop","$idHop");
    $pdoStat->execute();
    


    if ($ligne = $pdoStat->fetch(PDO::FETCH_NUM)) {
        
        $page = <<<HTML

    <h1>Requête</h1>
    <div class="shadow-md rounded-lg" style="width:250px;height:160px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT nomMed 
FROM Medecin 
WHERE idMed IN (
SELECT idMed 
FROM Service 
WHERE idHopital=$idHop
);
        </pre>
    </div>

HTML;
        $page .="   <h1>Résultat</h1>\n   <p>Les médecins pratiquant dans l'hopital $nomHop sont :</p>\n";
        $page .= <<<HTML
   <table class="table table-striped table-bordered" style="width:800px;">
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Ville</th>
            
        </tr>

HTML;

        $page.="       <tr>\n";
        $page.="           <td>$ligne[0]</td>\n";
        $page.="           <td>$ligne[1]</td>\n";
        $page.="           <td>$ligne[2]</td>\n";
        
        $page.="       </tr>\n";

        while($ligne = $pdoStat->fetch(PDO::FETCH_NUM)) {
            $page.="       <tr>\n";
            $page.="           <td>$ligne[0]</td>\n";
            $page.="           <td>$ligne[1]</td>\n";
            $page.="           <td>$ligne[2]</td>\n";
            
            $page.="       </tr>\n";        
        }
    $page .= "   </table>\n";
    } else {
        $page = <<<HTML

        <h1>Requête</h1>
        <div class="shadow-md rounded-lg" style="width:250px;height:160px; padding:3px;background-color:rgb(202, 197, 190);">
            <pre>
SELECT nomMed 
FROM medecin 
WHERE idMed IN (
    SELECT idMed 
    FROM service 
    WHERE idHopital=$idHop
    );
            </pre>
        </div>

HTML;
        $page .= "   <h1>Résultat</h1>\n   <p>Il n'y a aucun médecin dans l'hopital $nomHop.</p>\n";
    }
    $page .= <<<HTML
   <button class="btn btn-secondary" type="button" onclick="window.location.href='rechercher.php'" value="Nouvelle recherche">Nouvelle recherche</button>
HTML;
}
else 
{
    
    $sql = <<<SQL
    SELECT idHopital, nomHop
    FROM Hopital 
    
SQL;

    $pdo = $Requete->connect();
    $query = $pdo->query($sql);
    $req = $query->fetchAll();
    $res = [];

    $page = <<<HTML

    <h1>Recherche de médecins par hopital</h1>
    <p>
        Selectionnez un hopital.
    </p>
    <form action="rechercher.php" method="GET">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Hopital</label>
            </div>
            <select class="form-control" id="table" name="idHop" value="" required onchange="this.form.submit()">
                <option disabled selected value> -- Choisissez -- </option>

HTML;

    foreach ($req as $ide => $elem)
        {   
            $idHop = $elem['idHopital'];
            $nomHopital = $elem['nomHop'] . " ($idHop)";
            
            $page .=<<<HTML
                <option class="form-control" value="$idHop" name="$idHop">$nomHopital</option>
HTML;
            $page .="\n";
        };
    $page.=<<<HTML
            </select>
HTML;
    $conn = null;
    $page .="\n";
    $page .=<<<HTML
        </div>
    </form>
HTML;
}


$page .=<<<HTML

</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
 
