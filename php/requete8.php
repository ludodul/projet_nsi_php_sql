<?php
$Autor = "Groupe Tison-Pate-Dauchy";
$title = "Requete8";
require 'header.php';
require_once '../class/Requetes.class.php';

$Requete = new Requetes;
$pdo = $Requete->connect();

$req = <<<SQL
SELECT m.graviteMal, COUNT(g.idRem)
FROM Maladie m
    LEFT JOIN guerir g ON m.idMaladie = g.idMaladie
GROUP BY m.graviteMal ;
SQL;

try {
    $query = $pdo->query($req);    
    $page =<<<HTML

    <h1>Une requête avec agrégat et jointure externe</h1>
    <p>
        La liste des gravités de maladie avec le nombre de remèdes,
        éventuellement nul, soignant une gravité de maladie.
    </p>
    <h2>La requête</h2>
    <div class="shadow-md rounded-lg" style="width:440px;height:100px; padding:3px;background-color:rgb(202, 197, 190);">
        <pre>
SELECT m.graviteMal, COUNT(g.idRem)
FROM Maladie m
    LEFT JOIN guerir g ON m.idMaladie = g.idMaladie
GROUP BY m.graviteMal ;
        </pre>

    </div>
    <div>
        <h2>Les résultats</h2>
        <table class="table table-striped table-bordered" style="width:350px;">
            <tr>
                <th>Gravité Maladie</th>
                <th>Nombre de remède(s)</th>
                                
            </tr>

HTML;

    while($ligne = $query->fetch(PDO::FETCH_NUM)) {
        $page.="            <tr>\n";
        $page.="                <td>$ligne[0]</td>\n";
        $page.="                <td>$ligne[1]</td>\n";
        $page.="            </tr>\n";        
    }

    $page .=<<<HTML
        </table>
    </div>
</div>
</body>
</html>
HTML;
    echo $page;
    $pdo = NULL;
} 
catch (Exception $e) {
    echo "<p>ERREUR :".$e->getMessage()."</p>";
}