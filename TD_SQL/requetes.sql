USE Blanchisserie;
SET lc_time_names = 'fr_FR';


SELECT DISTINCT CpCli, villeCli
FROM CLIENT
ORDER BY 1,2;



SELECT CONCAT(nomEmp,"-",prnmEMP) EMPLOYE,
        qualif Qualification,
        salEmp salaire,
        DATE_FORMAT(dateEmb,"%b. %Y") Embauche
FROM EMPLOYE
WHERE salEmp BETWEEN 2000 AND 3000 AND villeEmp = "REIMS";


SELECT intLng Linge,
        IFNULL(ROUND(trfbacterio,2),"0") "Tarif Bactério"
FROM LINGE
WHERE intlng LIKE("%chirurgien%") OR trfbacterio is null;


SELECT CONCAT(CASE cvltEmp
                    WHEN "1" THEN "M"
                    WHEN "2" THEN "Mme"
                    WHEN "3" THEN "Mle"
                END,
                " ",
            nomEmp) EMPLOYE,
        qualif Qualification,
        DATE_FORMAT(dateNais,"%M %Y") "Date de Naissance"
FROM EMPLOYE
WHERE (qualif IN ("commercial","livreur","manoeuvre")) 
            AND (DATE_FORMAT(dateNais,"%Y") BETWEEN 1960 AND 1970);


SELECT CONCAT(nomEmp," ",prnmEMP) EMPLOYE,
        qualif Qualification,
        dateNais "Date de Naissance",
        dateEmb Embauche
FROM EMPLOYE
WHERE cvltEmp IN ("2","3")
    AND dateDpt IS NULL 
    AND DATE_FORMAT("2000-01-06","%Y-%D-%M") > dateEmb
ORDER BY 3;

SELECT villeCli ville,
        rsncli "Raison Sociale"
FROM CLIENT c
    INNER JOIN TYPE_CLIENT tc ON (c.cdtpcli = tc.cdtpcli)
WHERE tc.inttpcli IN ("RESTAURANT","HOTEL","CAFE")
ORDER BY 1,2;

SELECT CpCli "Ville", rsnCli "Raison Sociale"
FROM    CLIENT 
WHERE cdtpCli IN   (SELECT cdtpCli
                    FROM TYPE_CLIENT
                    WHERE intTpCli IN ("RESTAURANT","HOTEL","CAFE"))        
ORDER BY 1, 2 ;

SELECT DISTINCT CONCAT(nomEmp," - ",prnmEmp) AS "Employé",
        qualif "Qualification"
FROM EMPLOYE e 
    INNER JOIN TOURNEE t ON (e.cdEmp = t.cdEmp) ;

SELECT  CONCAT(nomEmp," - ",prnmEmp) AS "Employé",
        qualif "Qualification"
FROM EMPLOYE
WHERE cdEmp IN (SELECT cdEmp
               FROM TOURNEE) ;