-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql
-- Généré le :  mar. 10 déc. 2019 à 10:34
-- Version du serveur :  10.2.25-MariaDB
-- Version de PHP :  7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


-- --------------------------------------------------------

--
-- Structure de la table `AUTORISATION`
--

CREATE TABLE `AUTORISATION` (
  `CDEMP` char(3) NOT NULL,
  `CDVHC` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `AUTORISATION`
--

INSERT INTO `AUTORISATION` (`CDEMP`, `CDVHC`) VALUES
('018', '03'),
('018', '05'),
('018', '06'),
('018', '08'),
('096', '03'),
('096', '05'),
('096', '06'),
('200', '03');

-- --------------------------------------------------------

--
-- Structure de la table `CLIENT`
--

CREATE TABLE `CLIENT` (
  `CDCLI` char(5) NOT NULL,
  `CDTPCLI` char(2) DEFAULT NULL,
  `ADRCLI` varchar(35) DEFAULT NULL,
  `CPCLI` char(5) DEFAULT NULL,
  `VILLECLI` varchar(35) DEFAULT NULL,
  `CMPLADR` varchar(35) DEFAULT NULL,
  `RSNCLI` varchar(35) DEFAULT NULL,
  `TELCLI` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `CLIENT`
--

INSERT INTO `CLIENT` (`CDCLI`, `CDTPCLI`, `ADRCLI`, `CPCLI`, `VILLECLI`, `CMPLADR`, `RSNCLI`, `TELCLI`) VALUES
('02105', '15', '63 Rue M.C. Fouriaux', '51100', 'REIMS', NULL, 'PARCHIMY', '0326505656'),
('02776', '12', '2 Allée du Pont', '51100', 'REIMS', NULL, 'ADIS SA', '0326592095'),
('02988', '12', 'Chemin des Temples', '51170', 'FISMES', NULL, 'ECOTEC', '0326048262'),
('03042', '05', '33 Rue de Verdun', '51100', 'REIMS', NULL, 'REXIM S.A.', '0326810025'),
('03095', '12', '12 Rue Guillaume Dupré', '51100', 'REIMS', NULL, 'SONOCAS SARL', '0326800000'),
('03106', '12', 'ZISE Chemin de Saint-Léonard', '51100', 'REIMS', NULL, 'RESINOPLAST', '0326857500'),
('03137', '10', '52-72 Avenue Marechal Leclerc', '51430', 'TINQUEUX', 'BP 10', 'PETITJEAN S.A.', '0326713200'),
('03293', '12', 'Rue Voltaire', '51120', 'CLAMANGES', NULL, 'USC AEROSOLS', '0326273200'),
('03490', '14', '1 Avenue du Général de Gaulle', '51100', 'REIMS', NULL, 'EUROKERA SNC', '0326848500'),
('04020', '12', 'Rue Roger Dumoulin', '51100', 'REIMS', 'BP 1339', 'MGTS MECANIQUE', '0326542500'),
('04108', '12', '63 Rue Grouet', '02220', 'BRAINE', 'BP 2717', 'FORBO SA', '0323773030'),
('04336', '12', 'Quai du Canal', '51100', 'REIMS', 'BP 13', 'SMURFIT-SOCAR', '0326589015'),
('04743', '12', 'Rue de L\'Equipée', '51100', 'LA NEUVILETTE', NULL, 'LAMARD PLASTIQUES', '0326435200'),
('04834', '12', '24 Rue des Soranges', '51100', 'REIMS', NULL, 'MEGABOX SARL', '0326825550'),
('04996', '14', '54 Rue du Vieux Chemin de Pont', '51100', 'REIMS', NULL, 'AVH', '0326596500'),
('05348', '13', '44 Rue Paul Vaillant Couturier', '02220', 'BRAINE', NULL, 'SOCIETE J.R.', '0326937568'),
('05497', '12', 'Avenue du Général Patton', '51100', 'REIMS', 'BP 509', 'HENKEL ECOLAB SNC', '0326685511'),
('05567', '03', '12 Rue Moulin', '51430', 'TINQUEUX', 'ZI Est', 'GUENEE', '0326056987'),
('05580', '06', '80 Av Bourg', '51100', 'REIMS', NULL, 'CHARPENTIER', '0326362245'),
('05584', '02', '11 Cent. Com. des Blagis', '51120', 'SEZANNE', 'ZA', 'COUPE', '0326542196'),
('05587', '07', 'Cent. Com. Principal', '51370', 'ST BRICE', NULL, 'COCHET', '0326090247'),
('05596', '03', '66 Rue Gabriel Perri', '51420', 'CERNAY-LES-REIMS', NULL, 'LAFEUIL', '0326185614'),
('05599', '14', '19 Rue Ampere', '51370', 'ST BRICE', NULL, 'L I R E', '0326056932'),
('05610', '08', '33 Rue Bonte', '51430', 'TINQUEUX', NULL, 'CARREFOUR', '0326125687'),
('05631', '12', '84 Rue geoffroy', '51100', 'REIMS', NULL, 'ETS CRUCKE', '0326069874'),
('05637', '05', '5 Av des Violettes', '51370', 'ST BRICE', NULL, 'LEGAUDU', '0326125647'),
('05639', '08', '1 Bis Rue Pasteur', '51350', 'CORMONTREUIL', NULL, 'INTERMARCHE', '0326096354'),
('05640', '12', '21 Av Kennedy', '51350', 'CORMONTREUIL', NULL, 'SERVAL SA', '0326126541'),
('06056', '12', '2 Rue Clément Ader', '51120', 'LACHY', 'BP 1017', 'CHAMPAGNE FER', '0326786200'),
('06102', '12', 'Route de Châlons', '51100', 'REIMS', NULL, 'AXON\'CABLE', '0326817000'),
('06114', '12', '128 Avenue Jean Jaurès', '08300', 'RETHEL', 'BP 30', 'BRONZE ART', '0324386900'),
('06315', '13', 'Avenue Léon Charpentier', '51100', 'REIMS', 'BP 333', 'LOMMER', '0326298383'),
('06375', '12', 'Route d\'Eurville', '51100', 'REIMS', NULL, 'ISOROY FIBRES', '0326558900'),
('06565', '12', 'Rue de l\'Europe', '51100', 'REIMS', NULL, 'NOBEL PLASTIQUES', '0326736464'),
('06762', '12', 'Rue du Blanc Mont', '08300', 'SAULT-LES-RETHEL', 'BP 51', 'VALFOND MECANIQUE', '0324717077'),
('06817', '10', '34 Rue de Rilly la Montagne', '51100', 'REIMS', NULL, 'PROFITABLE SARL', '0326099657'),
('07173', '01', '18 Rue des Fusilles', '51100', 'REIMS', NULL, 'SERVICE VETERINAIRE', '0326528575'),
('07176', '14', '78 Av Coquelin', '51100', 'REIMS', NULL, 'DEFROICOURT', '0326053051'),
('23251', '13', 'Voie Balzac', '51100', 'REIMS', 'ZA Farmann', 'LES HORTENTIAS IV', '0326549650'),
('23712', '11', '100 Rue de Choissy', '51120', 'SEZANNE', NULL, 'HOTEL DES VOSGES', '0326106985'),
('52857', '01', '35 Bld Briand', '51100', 'REIMS', NULL, 'S A R L ABATTOIRS', '0326065478'),
('53666', '10', '1 Rue Duguesclin', '51420', 'CERNAY-LES-REIMS', NULL, 'LE DUGUESCLIN', '0326036954'),
('53674', '08', '14 Rue Gribelette', '51170', 'FISMES', 'ZI Nord', 'LECLERC SADISVIMA', '0326108529'),
('53844', '01', '74 Av Perrin', '08300', 'RETHEL', NULL, 'TRUCULUS', '0324528572'),
('54011', '10', '35 Av De Gaulle', '51100', 'REIMS', NULL, 'RESTAURANT DE LA POINTE', '0326056932'),
('54657', '13', '15 Rue Maillard', '51100', 'REIMS', NULL, 'LES HORTENTIAS II', '0326549651'),
('54784', '09', 'Av de la Gribelette', '51170', 'FISMES', NULL, 'CAFE LECLERC', '0326536740'),
('54965', '13', '1 Av Carnot', '51120', 'CHARLEVILLE', 'ZA Santos', 'S A F A A', '0326076532'),
('55204', '04', 'Pl du Moulin', '51430', 'TINQUEUX', NULL, 'LE POISSON BRILLANT', '0326054126'),
('55274', '09', '73 Av Rouget', '51420', 'CERNAY-LES-REIMS', NULL, 'BAR DU SUD', '0326182541'),
('55340', '04', 'Bois de l\'Epine', '51370', 'ST  LEONARD', 'ZI Ouest', 'CHAMPAGNE VIANDE', '0326528570'),
('55341', '02', '38 esplanade Fléchambault', '51100', 'REIMS', 'null', 'GOURMETS ET GOURMANDS ', '0326369462');

-- --------------------------------------------------------

--
-- Structure de la table `CONTRAT`
--

CREATE TABLE `CONTRAT` (
  `CDCTR` char(4) NOT NULL,
  `CDCLI` char(5) DEFAULT NULL,
  `CAUTION` float DEFAULT NULL,
  `DUREE` int(11) DEFAULT NULL,
  `DATECTR` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `CONTRAT`
--

INSERT INTO `CONTRAT` (`CDCTR`, `CDCLI`, `CAUTION`, `DUREE`, `DATECTR`) VALUES
('6023', '05348', 1718, 36, '2009-03-01'),
('6523', '04108', 1576, 36, '2009-05-01'),
('6598', '05596', 307.6, 36, '2009-07-01'),
('6694', '05639', 1636, 36, '2009-10-01'),
('6702', '02988', 2891, 36, '2009-11-01'),
('6711', '07173', 1870, 24, '2010-01-01'),
('6724', '03042', 701.6, 24, '2010-03-01'),
('6726', '06817', 1378, 24, '2010-04-01'),
('6731', '03106', 1750, 24, '2010-05-01'),
('6742', '03293', 1529, 24, '2010-06-01'),
('6750', '04996', 1183, 24, '2010-07-01'),
('6753', '06114', 1463, 24, '2010-08-01'),
('6758', '06762', 1750, 36, '2010-09-01'),
('6802', '55274', 1133, 24, '2011-01-01'),
('6805', '53666', 1430, 24, '2011-02-01'),
('7189', '53674', 2864, 36, '2010-09-01'),
('7367', '05637', 816.5, 24, '2010-02-01'),
('7472', '05640', 3487, 36, '2010-09-01'),
('7594', '23712', 4649, 24, '2010-08-01'),
('7807', '52857', 2871, 36, '2010-09-01'),
('7921', '05631', 3160, 36, '2010-05-01'),
('7945', '53844', 1529, 24, '2011-09-01'),
('8756', '05567', 779.6, 24, '2011-05-01'),
('9112', '05580', 882.5, 24, '2011-03-01'),
('9145', '23251', 4752, 36, '2010-09-01'),
('9213', '05640', 2970, 36, '2010-09-01'),
('9225', '53674', 1640, 36, '2010-09-01'),
('9365', '55204', 774.5, 36, '2010-06-01'),
('9417', '05610', 1636, 36, '2010-02-01'),
('9462', '05599', 680.7, 36, '2009-09-01'),
('9480', '05610', 1265, 36, '2010-02-01'),
('9602', '54784', 1368, 24, '2010-05-01'),
('9669', '05587', 420.4, 24, '2010-01-01'),
('9709', '54011', 1971, 36, '2010-10-01'),
('9850', '07176', 667.3, 24, '2011-06-01'),
('9875', '05584', 628.9, 36, '2011-07-01');

-- --------------------------------------------------------

--
-- Structure de la table `ECHANGE`
--

CREATE TABLE `ECHANGE` (
  `CDCTR` char(4) NOT NULL,
  `CDLNG` char(4) NOT NULL,
  `NBPC` int(11) DEFAULT NULL,
  `TAILLE` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ECHANGE`
--

INSERT INTO `ECHANGE` (`CDCTR`, `CDLNG`, `NBPC`, `TAILLE`) VALUES
('6023', '0001', 4, NULL),
('6023', '0260', 5, NULL),
('6023', '0378', 5, NULL),
('6023', '0385', 5, NULL),
('6023', '1347', 5, NULL),
('6523', '0274', 3, 'XL'),
('6523', '1511', 2, 'M'),
('6523', '2417', 3, 'XXL'),
('6598', '0251', 4, NULL),
('6598', '1090', 3, NULL),
('6598', '1113', 2, NULL),
('6694', '0210', 5, NULL),
('6694', '0251', 3, NULL),
('6694', '1090', 4, NULL),
('6694', '1113', 2, NULL),
('6694', '1141', 12, NULL),
('6702', '0170', 3, 'M'),
('6702', '0274', 5, 'XL'),
('6702', '1502', 4, 'S'),
('6702', '2095', 3, 'XL'),
('6711', '0200', 3, NULL),
('6711', '0210', 4, NULL),
('6711', '0251', 3, NULL),
('6711', '1090', 4, NULL),
('6711', '1113', 4, NULL),
('6724', '0210', 2, NULL),
('6724', '1090', 3, NULL),
('6724', '1141', 10, NULL),
('6726', '0180', 2, 'XL'),
('6726', '0366', 5, NULL),
('6726', '0446', 10, NULL),
('6726', '0929', 2, 'XL'),
('6726', '1141', 10, NULL),
('6726', '1555', 10, NULL),
('6726', '4568', 1, NULL),
('6731', '0274', 3, 'M'),
('6731', '2095', 2, 'XXL'),
('6731', '2417', 3, 'XL'),
('6742', '0274', 3, 'XXL'),
('6742', '1511', 2, 'XL'),
('6742', '2095', 3, 'XL'),
('6750', '0076', 5, 'M'),
('6750', '0153', 3, 'XL'),
('6750', '0424', 10, NULL),
('6753', '0116', 2, 'S'),
('6753', '0274', 2, 'XXL'),
('6753', '1511', 1, 'M'),
('6753', '2095', 3, 'XL'),
('6758', '0274', 3, 'XL'),
('6758', '2095', 2, 'M'),
('6758', '2417', 3, 'XL'),
('6802', '0842', 5, NULL),
('6802', '1060', 10, NULL),
('6802', '1572', 10, NULL),
('6802', '2805', 5, NULL),
('6805', '0184', 2, 'XL'),
('6805', '0836', 5, NULL),
('6805', '0929', 3, NULL),
('6805', '1096', 3, NULL),
('6805', '1141', 10, NULL),
('6805', '4879', 1, NULL),
('7189', '0170', 4, 'XL'),
('7189', '0180', 2, 'M'),
('7189', '0184', 2, 'XL'),
('7189', '0929', 3, 'S'),
('7189', '1502', 2, 'S'),
('7189', '1511', 3, 'XL'),
('7367', '0170', 2, 'XL'),
('7367', '0251', 2, NULL),
('7367', '1090', 2, NULL),
('7367', '1113', 2, NULL),
('7367', '1141', 10, NULL),
('7367', '1511', 2, 'L'),
('7472', '1509', 4, 'S'),
('7472', '2417', 10, 'L'),
('7594', '1083', 10, NULL),
('7594', '1122', 2, NULL),
('7594', '1181', 2, NULL),
('7594', '1341', 12, NULL),
('7594', '1347', 22, NULL),
('7594', '2051', 10, NULL),
('7807', '0200', 6, NULL),
('7807', '0210', 6, NULL),
('7807', '1090', 6, NULL),
('7807', '1142', 6, NULL),
('7807', '1348', 4, NULL),
('7921', '1502', 5, 'M'),
('7921', '2417', 10, 'XL'),
('7945', '0200', 3, NULL),
('7945', '0210', 3, NULL),
('7945', '0251', 3, NULL),
('7945', '1090', 3, NULL),
('7945', '1113', 3, NULL),
('8756', '0180', 2, 'M'),
('8756', '0184', 2, 'XL'),
('8756', '0929', 1, 'M'),
('8756', '1141', 10, NULL),
('9112', '0081', 5, 'M'),
('9112', '0251', 5, NULL),
('9112', '1141', 10, NULL),
('9145', '0001', 10, NULL),
('9145', '0015', 5, NULL),
('9145', '0018', 5, NULL),
('9145', '0076', 10, 'M'),
('9145', '0153', 4, 'XL'),
('9145', '0255', 3, NULL),
('9145', '0260', 5, NULL),
('9145', '1346', 5, NULL),
('9145', '1347', 5, NULL),
('9145', '1724', 3, NULL),
('9145', '1725', 2, NULL),
('9213', '0001', 4, NULL),
('9213', '0015', 5, NULL),
('9213', '0018', 5, NULL),
('9213', '0197', 4, NULL),
('9213', '1174', 4, NULL),
('9213', '1346', 10, NULL),
('9213', '1347', 10, NULL),
('9225', '0081', 3, 'M'),
('9225', '0180', 2, 'XL'),
('9225', '0184', 3, 'XXL'),
('9225', '0929', 2, 'XL'),
('9225', '1096', 3, NULL),
('9225', '1141', 10, NULL),
('9365', '0184', 3, 'XL'),
('9365', '1096', 3, NULL),
('9365', '1113', 2, NULL),
('9365', '1141', 8, NULL),
('9417', '0210', 5, NULL),
('9417', '0251', 3, NULL),
('9417', '1090', 4, NULL),
('9417', '1113', 2, NULL),
('9417', '1141', 12, NULL),
('9462', '0001', 2, NULL),
('9462', '0076', 2, 'S'),
('9462', '0153', 3, 'XL'),
('9462', '0260', 2, NULL),
('9480', '0180', 2, 'XL'),
('9480', '0184', 2, 'M'),
('9480', '0929', 2, 'XL'),
('9480', '1090', 2, NULL),
('9480', '1094', 3, NULL),
('9480', '1113', 3, NULL),
('9480', '1141', 8, NULL),
('9602', '0173', 2, 'XL'),
('9602', '0848', 5, NULL),
('9602', '1141', 10, NULL),
('9602', '1227', 3, 'M'),
('9602', '4896', 1, NULL),
('9669', '0170', 2, 'XL'),
('9669', '1094', 2, NULL),
('9669', '1141', 10, NULL),
('9709', '0446', 10, NULL),
('9709', '0836', 5, NULL),
('9709', '0848', 5, NULL),
('9709', '1060', 12, NULL),
('9709', '1096', 2, NULL),
('9709', '1141', 10, NULL),
('9709', '2805', 5, NULL),
('9709', '4896', 1, NULL),
('9850', '0076', 4, 'M'),
('9850', '0153', 2, 'XL'),
('9850', '0260', 2, NULL),
('9875', '0200', 3, NULL),
('9875', '0251', 2, NULL),
('9875', '1090', 3, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `EMPLOYE`
--

CREATE TABLE `EMPLOYE` (
  `CDEMP` char(3) NOT NULL,
  `CDSUP` char(3) DEFAULT NULL,
  `NOMEMP` varchar(35) DEFAULT NULL,
  `PRNMEMP` varchar(15) DEFAULT NULL,
  `CVLTEMP` char(1) DEFAULT NULL,
  `DATENAIS` date DEFAULT NULL,
  `ADREMP` varchar(35) DEFAULT NULL,
  `CPEMP` char(5) DEFAULT NULL,
  `VILLEEMP` varchar(35) DEFAULT NULL,
  `TELEMP` char(10) DEFAULT NULL,
  `DATEEMB` date DEFAULT NULL,
  `DATEDPT` date DEFAULT NULL,
  `QUALIF` varchar(35) DEFAULT NULL,
  `SALEMP` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `EMPLOYE`
--

INSERT INTO `EMPLOYE` (`CDEMP`, `CDSUP`, `NOMEMP`, `PRNMEMP`, `CVLTEMP`, `DATENAIS`, `ADREMP`, `CPEMP`, `VILLEEMP`, `TELEMP`, `DATEEMB`, `DATEDPT`, `QUALIF`, `SALEMP`) VALUES
('005', NULL, 'CHEVRIER', 'Sylvie', '2', '1956-05-26', '47 Rue CERES', '51100', 'REIMS', '0326048921', '1985-03-01', '1989-08-30', 'DIRECTEUR', 4200),
('006', '008', 'HERMANN', 'Jean', '1', '1980-06-13', '6 Bd Zola', '51100', 'REIMS', NULL, '1998-06-01', NULL, 'VENDEUR', 1650),
('007', '005', 'FAVERGER', 'Christian', '1', '1970-06-26', '7 Av Mermoz', '51100', 'REIMS', NULL, '1999-07-01', NULL, 'RESPONSABLE DES VENTES', 2900),
('008', '007', 'SAMAIN', 'Clotilde', '2', '1960-03-30', '8 Pl A Briand', '51100', 'REIMS', NULL, '2000-05-01', NULL, 'COMMERCIAL', 2500),
('010', '007', 'MOREL', 'Jeanne', '2', '1968-01-28', '9 Av V Hugo', '51500', 'TAISSY', NULL, '2001-05-01', NULL, 'COMMERCIAL', 2000),
('018', '145', 'LAGIER', 'Claude', '1', '1962-03-24', '41 Rue ARTISANS', '51350', 'CORMONTREUIL', '0326054624', '1987-10-01', NULL, 'LIVREUR', 1850),
('021', '085', 'LELONG', 'Nicolas', '1', '1950-11-08', '22 rue CHEDEL', '51370', 'ST BRICE', '0326605143', NULL, NULL, 'MANOEUVRE', 1450),
('045', '085', 'THIRIET', 'Arnaud', '1', '1951-12-26', '39 Av EPERNAY', '51100', 'REIMS', '0326075121', '1995-12-01', NULL, 'MANOEUVRE', 1500),
('056', '145', 'BARBELIN', 'Linda', '2', '1969-01-17', '17 Rue ARSENAL', '51100', 'CERNAY-LES-REIMS', '0326051456', NULL, NULL, 'SECRETAIRE', 2100),
('085', '145', 'DAUTEL', 'Richard', '1', '1952-09-26', '15 Av BRANLY', '51100', 'REIMS', '0326056247', '1995-10-01', NULL, 'CONTREMAITRE', 3000),
('096', '085', 'MANZONI', 'Stephanie', '3', '1968-04-12', '4 Pl BRAQUE', '51100', 'REIMS', '0326149603', '1996-03-01', NULL, 'LIVREUR', 1700),
('145', NULL, 'LAMANT', 'Serge', '1', '1960-07-04', '5 Bd ORADOUR', '51430', 'TINQUEUX', '0326026829', '1989-09-01', NULL, 'DIRECTEUR', 5500),
('156', '145', 'REVEL', 'Carole', '3', '1976-03-15', '34 Rue GUERIN', '51430', 'TINQUEUX', '0326054569', '1997-09-01', NULL, 'COMMERCIAL', 2750),
('200', '096', 'SANCHEZ', 'Yohan', '1', '1988-12-10', '10 rue du point du jour', '51100', 'REIMS', NULL, NULL, NULL, 'LIVREUR', 1300),
('201', '045', 'HONORE', 'Perrine', '3', '1970-09-15', '5 place Toulouse Lautrec', '51100', 'REIMS', '0612474849', '2016-09-05', NULL, 'MANOEUVRE', 1300);

-- --------------------------------------------------------

--
-- Structure de la table `LINGE`
--

CREATE TABLE `LINGE` (
  `CDLNG` char(4) NOT NULL,
  `CDTPLNG` char(2) DEFAULT NULL,
  `INTLNG` varchar(35) DEFAULT NULL,
  `TRFABN` float DEFAULT NULL,
  `TRFBACTERIO` float DEFAULT NULL,
  `TRFIMP` float DEFAULT NULL,
  `TRFNTG` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `LINGE`
--

INSERT INTO `LINGE` (`CDLNG`, `CDTPLNG`, `INTLNG`, `TRFABN`, `TRFBACTERIO`, `TRFIMP`, `TRFNTG`) VALUES
('0001', '03', 'ALEZE 110 X 170', 5.42, 2.17, 59.9, 10.9),
('0015', '03', 'CHAMP', 3.33, 0.88, 29.6, 4.38),
('0018', '03', 'CHAMP DOUBLE', 3.67, 1.09, 66.1, 5.42),
('0076', '02', 'BLOUSE INFIRMIERE', 8.19, NULL, 106, 13.4),
('0081', '02', 'BLOUSE MICHELINE', 8.15, NULL, 145, 14.5),
('0116', '02', 'BLOUSE BORIS', 7.95, NULL, 139, 12.9),
('0153', '02', 'BLOUSE MEDECIN', 7.98, NULL, 105, 13.3),
('0170', '02', 'VESTE AQUITAINE', 6.34, NULL, 130, 11.4),
('0173', '02', 'VESTE BARMAN', 6.91, NULL, 144, 10.9),
('0180', '02', 'VESTE CUISINIER', 6.91, NULL, 134, 10.9),
('0184', '02', 'VESTE TRAITEUR', 6.91, NULL, 147, 10.9),
('0197', '03', 'BOTTE DOCTEUR', 2.77, 1.19, 27.6, 5.95),
('0200', '09', 'BOURGERON', 14.4, 4.3, 154, 21.5),
('0210', '09', 'BOUBOU', 24.9, 6.37, 238, 31.9),
('0251', '09', 'CALOT BLANC', 2.63, 0.77, 14, 3.85),
('0255', '03', 'CALOT CHIRURGIEN', 2.98, 1.12, 14, 5.6),
('0260', '03', 'COIFFE INFIRMIERE', 6.3, 1.47, 16.4, 7.35),
('0274', '02', 'COMBINAISON BUGATTI', 12.8, NULL, 226, 18.5),
('0337', '05', 'COUVERTURE COUNTRY', 3.15, NULL, 26.9, 5.6),
('0366', '06', 'NAPPE METIS 10 X 10', 5.6, NULL, 59.9, 9.45),
('0378', '03', 'DESSUS DE LIT', 12.2, 3.59, 131, 17.9),
('0385', '03', 'DRAP COTON PETIT', 8.05, 3.04, 103, 15.2),
('0424', '08', 'SERVIETTE EPONGE', 2.63, NULL, 33.7, 3.85),
('0446', '04', 'ESSUIE-VERRES', 2.63, NULL, 16.4, 4.55),
('0836', '06', 'NAPPE LINELIS 15 X 20', 6.13, NULL, 69, 10.4),
('0842', '06', 'NAPPE CALYPSO 20 X 20', 7, NULL, 73.7, 10.7),
('0848', '06', 'NAPPE COUNTRY 20 X 50', 7.44, NULL, 84.4, 11.4),
('0929', '02', 'PANTALON CUISINIER', 6.91, NULL, 130, 10.9),
('1060', '05', 'SERVIETTE METIS', 2.89, NULL, 34.1, 5.42),
('1076', '07', 'SERVIETTE OEIL', 2.19, NULL, 14.3, 3.5),
('1083', '07', 'SERVIETTE LAVABO', 3.06, NULL, 21.2, 5.16),
('1090', '09', 'TABLIER BOUCHER', 7.35, 2.87, 45.9, 14.4),
('1094', '04', 'TABLIER', 7.35, NULL, 36.9, 9.8),
('1096', '04', 'TABLIER CHEF', 8.31, NULL, 49.4, 9.8),
('1113', '09', 'TABLIER SERPILLIERE', 9.19, 3.25, 57, 16.3),
('1122', '07', 'TABLIER VALET', 7.26, NULL, 45.8, 8.75),
('1141', '04', 'TORCHON', 2.1, NULL, 8.75, 3.67),
('1142', '09', 'TORCHON', 2.63, 1.37, 8.93, 6.83),
('1174', '03', 'TRAVERSIN', 3.41, 0.875, 24.9, 4.38),
('1181', '07', 'TABLIER CHAMBRE', 7.88, NULL, 52.9, 8.75),
('1227', '02', 'VESTE GARCON', 6.91, NULL, 158, 10.9),
('1341', '07', 'DRAP SUPERLIS PETIT', 10.6, NULL, 130, 17.9),
('1346', '03', 'DRAP COTON GRAND', 10.9, 3.61, 158, 18),
('1347', '03', 'TAIE COTON', 3.5, 1.12, 45.9, 5.6),
('1348', '07', 'TAIE COTON', 3.5, NULL, 46.5, 5.6),
('1502', '02', 'BLOUSE PICARDIE ML', 7.44, NULL, 190, 14.4),
('1509', '02', 'BLOUSE PICARDIE MC', 7, NULL, 182, 14),
('1511', '02', 'PANTALON ARTOIS', 8.19, NULL, 118, 13.4),
('1555', '05', 'SNACK COUNTRY', 2.19, NULL, 14.4, 3.5),
('1572', '05', 'SERVIETTE COUNTRY', 2.63, NULL, 24.9, 4.81),
('1600', '08', 'PEIGNOIR DE BAIN', 14.4, NULL, 183, 21.9),
('1718', '08', 'TAPIS DE BAIN', 5.08, NULL, 56.7, 5.6),
('1724', '03', 'TUNIQUE CHIRURGIEN', 12.4, 3.31, 87.9, 16.5),
('1725', '03', 'PANTALON CHIRURGIEN', 12.6, 3.65, 87.5, 18.2),
('2051', '07', 'DRAP SUPERLIS GRAND', 12, NULL, 167, 18.8),
('2095', '02', 'COTTE ROYAL', 11.9, NULL, 205, 17.4),
('2417', '02', 'COMBINAISON ANJOU', 11.6, NULL, 221, 16.6),
('2805', '05', 'NAPPERON', 2.8, NULL, 35, 5.6),
('4568', '01', 'RM 15', 21.8, NULL, 156, 38.9),
('4879', '01', 'VISIOMATIC', 25.4, NULL, 167, 42.5),
('4896', '01', 'RM 4', 17.9, NULL, 98.3, 35.4);

-- --------------------------------------------------------

--
-- Structure de la table `PASSAGE`
--

CREATE TABLE `PASSAGE` (
  `CDCTR` char(4) NOT NULL,
  `CDTRN` char(2) NOT NULL,
  `NUMRPSG` int(11) DEFAULT NULL,
  `HPSG` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `PASSAGE`
--

INSERT INTO `PASSAGE` (`CDCTR`, `CDTRN`, `NUMRPSG`, `HPSG`) VALUES
('6023', '07', 8, 14.3),
('6523', '07', 7, 14),
('6598', '09', 2, 9.15),
('6694', '07', 11, 16),
('6702', '07', 3, 10),
('6711', '07', 12, 16.3),
('6711', '09', 7, 14),
('6724', '09', 9, 15),
('6726', '01', 9, 17),
('6731', '02', 10, 15.3),
('6742', '09', 8, 14.3),
('6750', '02', 11, 16),
('6753', '09', 6, 11.3),
('6758', '09', 5, 11.1),
('6802', '09', 1, 9),
('6805', '09', 3, 9.3),
('7189', '02', 4, 10.3),
('7189', '07', 6, 11),
('7367', '07', 1, 8.3),
('7472', '01', 7, 16),
('7594', '01', 5, 14.3),
('7594', '07', 13, 17),
('7807', '02', 1, 9),
('7921', '01', 3, 10.3),
('7945', '09', 4, 10.4),
('8756', '09', 11, 16),
('9112', '01', 1, 9.3),
('9145', '01', 2, 10),
('9213', '02', 9, 15),
('9225', '02', 3, 10),
('9225', '07', 5, 10.4),
('9365', '07', 2, 9),
('9417', '09', 10, 15.3),
('9462', '02', 7, 12),
('9602', '02', 5, 11),
('9602', '07', 4, 10.3),
('9669', '02', 8, 14.3),
('9709', '02', 2, 9.3),
('9850', '01', 8, 16.3),
('9875', '01', 6, 15);

-- --------------------------------------------------------

--
-- Structure de la table `TOURNEE`
--

CREATE TABLE `TOURNEE` (
  `CDTRN` char(2) NOT NULL,
  `CDEMP` char(3) DEFAULT NULL,
  `CDVHC` char(3) DEFAULT NULL,
  `CDJR` int(11) DEFAULT NULL,
  `HDEBTRN` float DEFAULT NULL,
  `HFINTRN` float DEFAULT NULL,
  `KMTRN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `TOURNEE`
--

INSERT INTO `TOURNEE` (`CDTRN`, `CDEMP`, `CDVHC`, `CDJR`, `HDEBTRN`, `HFINTRN`, `KMTRN`) VALUES
('01', '018', '05', 2, 9, 17, 200),
('02', '096', '06', 2, 8.3, 16.3, 250),
('03', '018', '03', 3, 9, 16, 210),
('04', '018', '03', 4, 7, 17, 300),
('05', '096', '08', 3, 9, 15, 200),
('06', '200', '06', 5, 10, 17, 150),
('07', '096', '06', 6, 8, 17, 230),
('08', '096', '05', 4, 8, 16, 250),
('09', '018', '05', 6, 8, 17, 240),
('10', '096', '03', 5, 7.3, 18, 300);

-- --------------------------------------------------------

--
-- Structure de la table `TYPE_CLIENT`
--

CREATE TABLE `TYPE_CLIENT` (
  `CDTPCLI` char(2) NOT NULL,
  `INTTPCLI` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `TYPE_CLIENT`
--

INSERT INTO `TYPE_CLIENT` (`CDTPCLI`, `INTTPCLI`) VALUES
('01', 'ABATTOIR'),
('02', 'BOUCHERIE'),
('03', 'CHARCUTERIE'),
('04', 'POISSONNERIE'),
('05', 'VOLAILLER'),
('06', 'BOULANGERIE'),
('07', 'PATISSERIE'),
('08', 'HYPERMARCHE'),
('09', 'CAFE'),
('10', 'RESTAURANT'),
('11', 'HOTEL'),
('12', 'ATELIER'),
('13', 'CLINIQUE'),
('14', 'LABORATOIRE D\'ANALYSE'),
('15', 'LABORATOIRE DE CHIMIE'),
('16', 'FROMAGERIE'),
('17', 'CREMERIE');

-- --------------------------------------------------------

--
-- Structure de la table `TYPE_LINGE`
--

CREATE TABLE `TYPE_LINGE` (
  `CDTPLNG` char(2) NOT NULL,
  `CDTRMNT` char(1) DEFAULT NULL,
  `INTTPLNG` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `TYPE_LINGE`
--

INSERT INTO `TYPE_LINGE` (`CDTPLNG`, `CDTRMNT`, `INTTPLNG`) VALUES
('01', NULL, 'ESSUIE-MAINS'),
('02', NULL, 'VETEMENT DE TRAVAIL'),
('03', 'B', 'SANTE'),
('04', NULL, 'LINGE DE CUISINE'),
('05', NULL, 'LINGE DE TABLE'),
('06', NULL, 'NAPPE'),
('07', NULL, 'HOTELLERIE-RESTAURATION'),
('08', NULL, 'HYGIENE'),
('09', 'B', 'COMMERCE ALIMENTAIRE'),
('10', NULL, 'VETEMENT DE CUISINE');

-- --------------------------------------------------------

--
-- Structure de la table `VEHICULE`
--

CREATE TABLE `VEHICULE` (
  `CDVHC` char(3) NOT NULL,
  `MODVHC` varchar(35) DEFAULT NULL,
  `VOLVHC` int(11) DEFAULT NULL,
  `DATMVHC` date DEFAULT NULL,
  `IMMVHC` varchar(10) DEFAULT NULL,
  `KMVHC` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `VEHICULE`
--

INSERT INTO `VEHICULE` (`CDVHC`, `MODVHC`, `VOLVHC`, `DATMVHC`, `IMMVHC`, `KMVHC`) VALUES
('03', 'PEUGEOT J7', 14, '2000-03-14', '1839ST51', 148520),
('05', 'RENAULT TRAFIC', 14, '1999-06-25', '1056TP51', 96502),
('06', 'PEUGEOT EXPERT', 20, '2003-10-18', '2657TB51', 56478),
('08', 'FORD TRANSIT', 12, '2001-11-14', '2157TR51', 62147);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `AUTORISATION`
--
ALTER TABLE `AUTORISATION`
  ADD PRIMARY KEY (`CDEMP`,`CDVHC`),
  ADD KEY `FK_CDVHC` (`CDVHC`);

--
-- Index pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
  ADD PRIMARY KEY (`CDCLI`),
  ADD KEY `FK_TPCLI` (`CDTPCLI`);

--
-- Index pour la table `CONTRAT`
--
ALTER TABLE `CONTRAT`
  ADD PRIMARY KEY (`CDCTR`),
  ADD KEY `FK_CDCLI` (`CDCLI`);

--
-- Index pour la table `ECHANGE`
--
ALTER TABLE `ECHANGE`
  ADD PRIMARY KEY (`CDCTR`,`CDLNG`),
  ADD KEY `FK_CDLNG` (`CDLNG`);

--
-- Index pour la table `EMPLOYE`
--
ALTER TABLE `EMPLOYE`
  ADD PRIMARY KEY (`CDEMP`),
  ADD KEY `FK_CDSUP` (`CDSUP`);

--
-- Index pour la table `LINGE`
--
ALTER TABLE `LINGE`
  ADD PRIMARY KEY (`CDLNG`),
  ADD KEY `FL_CDTPLINGE` (`CDTPLNG`);

--
-- Index pour la table `PASSAGE`
--
ALTER TABLE `PASSAGE`
  ADD PRIMARY KEY (`CDCTR`,`CDTRN`),
  ADD KEY `FK_CDTRN` (`CDTRN`);

--
-- Index pour la table `TOURNEE`
--
ALTER TABLE `TOURNEE`
  ADD PRIMARY KEY (`CDTRN`),
  ADD KEY `FK_EMP` (`CDEMP`),
  ADD KEY `FK_VHC` (`CDVHC`);

--
-- Index pour la table `TYPE_CLIENT`
--
ALTER TABLE `TYPE_CLIENT`
  ADD PRIMARY KEY (`CDTPCLI`);

--
-- Index pour la table `TYPE_LINGE`
--
ALTER TABLE `TYPE_LINGE`
  ADD PRIMARY KEY (`CDTPLNG`);

--
-- Index pour la table `VEHICULE`
--
ALTER TABLE `VEHICULE`
  ADD PRIMARY KEY (`CDVHC`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `AUTORISATION`
--
ALTER TABLE `AUTORISATION`
  ADD CONSTRAINT `FK_CDEMP` FOREIGN KEY (`CDEMP`) REFERENCES `EMPLOYE` (`CDEMP`),
  ADD CONSTRAINT `FK_CDVHC` FOREIGN KEY (`CDVHC`) REFERENCES `VEHICULE` (`CDVHC`);

--
-- Contraintes pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
  ADD CONSTRAINT `FK_TPCLI` FOREIGN KEY (`CDTPCLI`) REFERENCES `TYPE_CLIENT` (`CDTPCLI`);

--
-- Contraintes pour la table `CONTRAT`
--
ALTER TABLE `CONTRAT`
  ADD CONSTRAINT `FK_CDCLI` FOREIGN KEY (`CDCLI`) REFERENCES `CLIENT` (`CDCLI`);

--
-- Contraintes pour la table `ECHANGE`
--
ALTER TABLE `ECHANGE`
  ADD CONSTRAINT `FK_CDCTR` FOREIGN KEY (`CDCTR`) REFERENCES `CONTRAT` (`CDCTR`),
  ADD CONSTRAINT `FK_CDLNG` FOREIGN KEY (`CDLNG`) REFERENCES `LINGE` (`CDLNG`);

--
-- Contraintes pour la table `EMPLOYE`
--
ALTER TABLE `EMPLOYE`
  ADD CONSTRAINT `FK_CDSUP` FOREIGN KEY (`CDSUP`) REFERENCES `EMPLOYE` (`CDEMP`);

--
-- Contraintes pour la table `LINGE`
--
ALTER TABLE `LINGE`
  ADD CONSTRAINT `FL_CDTPLINGE` FOREIGN KEY (`CDTPLNG`) REFERENCES `TYPE_LINGE` (`CDTPLNG`);

--
-- Contraintes pour la table `PASSAGE`
--
ALTER TABLE `PASSAGE`
  ADD CONSTRAINT `FK_CDCTR1` FOREIGN KEY (`CDCTR`) REFERENCES `CONTRAT` (`CDCTR`),
  ADD CONSTRAINT `FK_CDTRN` FOREIGN KEY (`CDTRN`) REFERENCES `TOURNEE` (`CDTRN`);

--
-- Contraintes pour la table `TOURNEE`
--
ALTER TABLE `TOURNEE`
  ADD CONSTRAINT `FK_EMP` FOREIGN KEY (`CDEMP`) REFERENCES `EMPLOYE` (`CDEMP`),
  ADD CONSTRAINT `FK_VHC` FOREIGN KEY (`CDVHC`) REFERENCES `VEHICULE` (`CDVHC`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
