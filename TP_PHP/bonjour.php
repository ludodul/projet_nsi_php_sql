<?php

if (!session_id()) session_start(); // Ça ne sert à rien mais ce sera bien amusant après

$nom = "inconnu" ;
if (isset($_GET['nom']) && !empty($_GET['nom'])) {
    $nom = htmlentities($_GET['nom']) ;
}

$html = <<<HTML
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Salutations</title>
      
      <style type='text/css'>
      table {
        border-spacing  : 0 ;
        border-collapse : collapse ;
      }
      td.nombre {
        text-align : right ;
      }
      </style>
    </head>
    <body>
    <div id='page'>
    <h1>Bonjour&nbsp;!</h1>
        <p>Bonjour {$nom}, j'espère que vous passez une bonne journée.
    </div>
    </body>
</html>
HTML;

echo $html ;