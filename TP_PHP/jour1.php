<?php
$page = <<<HTML
<!doctype html>
<html lang = 'fr'>
    <head>
        <meta charset="utf-8">
        <title>Multiplication</title>
    </head>
<body>

HTML;

$liste = [
    1 => "Lundi",
    2 => "Mardi",
    3 => "Mercredi",
    4 => "Jeudi",
    5 => "Vendredi",
    6 => "Samedi",
    7 => "Dimanche",
];

$formulaire_head = <<<HTML
<form action="voirjour.php" method="POST">
    <select name="day" required>
        <option value="">Choisissez</option>
HTML;
$formulaire_body;
foreach ($liste as $numb => $day){
    $formulaire_body .= <<<HTML
<option value="$numb">$day</option>
HTML;
};
        
$formulaire_foot = <<<HTML
   </select>
    <button type=submit>Envoyer</button>
</form>
HTML;
$page_footer = <<<HTML
</body>
HTML;

echo $page.$formulaire_head.$formulaire_body.$formulaire_foot.$page_footer;