<?php
$page = <<<HTML
<!doctype html>
<html lang = 'fr'>
    <head>
        <meta charset="utf-8">
        <title>Multiplication</title>
    </head>
<body>

HTML;

$res = "";
$mult = $_GET['n'];
$res .= is_integer($mult);

if (!ctype_digit($mult)){
        header("Location: http://localhost:8080/TP_PHP/nombre.html");
        die();
    };

$res .= gettype($mult); 

for ( $i=0 ; $i<=10 ; $i++ )
{
    $prod = $i * $mult;  
    if ($i<=9){ 
        $res .= <<<HTML
        <div> <pre> $i x $mult = $prod</pre></div>

HTML;
    }
    else{
        $res .= <<<HTML
        <div><pre>$i x $mult = $prod</pre></div>

HTML;
    }
};
$res .= <<<HTML
</body>
HTML;

echo $page.$res;