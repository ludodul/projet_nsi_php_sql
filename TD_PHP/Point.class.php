<?php
class Point
{
    private $x;
    private $y;

    function __construct(float $x=0,float $y=0)
    {
        $this->x = $x;
        $this->y = $y;
    }
    public function getX():float
    {
        return $this->x;
    }
    public function getY():float
    {
        return $this->y;
    }
    public function __toString()
    {
        $res = "x : {$this->getX()}\n";
        $res = $res."y : {$this->getY()}\n";
        return $res;
    }
    public function translation(float $dx,float $dy):void
    {
        $this->x = $this->x + $dx;
        $this->y = $this->y + $dy;
    }
}