<?php

class Person 
{
    private $lastName; //string
    private $firstName; //string
    private $age; //int
    
    

    function __construct(string $lastName='Doe',string $firstName='John',int $age=123)
    {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->age = $age;
        
    }

    public function print() : void
    {
        echo "Nom : $this->lastName \n";
    }
    public function __toString() : string
    {
        $res = "Nom : $this->lastName\n";
        $res = $res."Prénom : $this->firstName\n";
        $res = $res."Age : $this->age\n";
        return $res;
    }

}