<?php
require_once "Country.class.php";

$france = new Country("France",641185,66600000);
$italie = new Country("Italie",'301336.0','60626442');
$noNameCountry = new Country;
$incompleteCountry = new Country("pays",123456);
$newFrance = copie($france);

function copie(Country $country)
{
    return clone $country;
}

function printCountry(Country $country)
{
    $name = $country->getName();
    $surface = $country->getSurface();
    $population = $country->getPopulation();
    return "name : $name\nsurface : $surface\npopulation : $population \n";
}


echo printCountry($newFrance);

