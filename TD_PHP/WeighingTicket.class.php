<?php
class WeighingTicket
{
    private $articleName;
    private $pricePerKilogram;
    private $weight;
    private $price;

    function __construct(string $articleName='Unknown',float $pricePerKilogram=0,int $weight=0)
    {
        $this->articleName = $articleName;
        $this->pricePerKilogram = $pricePerKilogram;
        $this->weight = $weight;
        $this->price = $this->calculPrice($this->pricePerKilogram,$this->weight);
    }
    private function calculPrice(float $pricePerKilogram,int $weight)
    {
        return $this->weight * $this->pricePerKilogram / 1000;
    }
    public function getArticleName()
    {
        return $this->articleName;
    }

    public function getPricePerKilogram()
    {
        return $this->pricePerKilogram;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function setArticleName(string $newName) : void
    {
        $this->articleName = $newName;
    }

    public function setPricePerKilogram(float $newPricePerKilo) : void
    {
        $this->pricePerKilogram = $newPricePerKilo;
        $this->price = $this->calculPrice($this->pricePerKilogram,$this->weight);
    }

    public function setWeight(int $newWeight) : void
    {
        $this->weight = $newWeight;
        $this->price = $this->calculPrice($this->pricePerKilogram,$this->weight);
    }
    public function __toString()
    {
        $res = "*******************************\n";
        $res = $res."* Article      : {$this->getArticleName()}\n";
        $res = $res."* Prix au kilo : {$this->getPricePerKilogram()}\n";
        $res = $res."* Poids (gr)   : {$this->getWeight()}\n";
        $res = $res."* \n";
        $res = $res."* Prix         : {$this->getPrice()}\n";
        $res = $res."*******************************\n";
        return $res;
    }
    public function weighing():void
    {
        $pds = rand(0,5000);
        $this->setWeight($pds);
    }

    
}
